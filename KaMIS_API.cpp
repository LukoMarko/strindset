#include "KaMIS_API.hh"

std::vector<bool> createIndSet_OMIS(graph_access& G, graph_access& fG) {
    mis_log::instance()->restart_total_timer();
    mis_log::instance()->print_title();

    MISConfig mis_config;

    double timeLimit = (double)G.number_of_nodes()/100.0;
    // Set Configuration
    // Basic
    mis_config.population_size                        = 50;
    mis_config.repetitions                            = 50;
    mis_config.time_limit                             = timeLimit;
    // KaHIP
    mis_config.kahip_mode                             = 0; //FAST
    // Randomization
    mis_config.seed                                   = 0;
    mis_config.diversify                              = true;
    mis_config.imbalance                              = 0.03;
    mis_config.randomize_imbalance                    = true;
    // Selection
    mis_config.enable_tournament_selection            = true;
    mis_config.tournament_size                        = 2;
    // Mutation
    mis_config.flip_coin                              = 1;
    // Combination
    mis_config.use_hopcroft                           = false;
    mis_config.optimize_candidates                    = true;
    // Multiway
    mis_config.use_multiway_vc                        = false;
    mis_config.multiway_blocks                        = 64;
    // Thresholds
    mis_config.insert_threshold                       = 150;
    mis_config.pool_threshold                         = 250;
    mis_config.pool_renewal_factor                    = 10.0;
    // Separator pool
    mis_config.number_of_separators                   = 10;
    mis_config.number_of_partitions                   = 10;
    mis_config.number_of_k_separators                 = 10;
    mis_config.number_of_k_partitions                 = 10;
    // Output
    mis_config.print_repetition                       = true;
    mis_config.print_population                       = true;
    mis_config.console_log                            = true;
    mis_config.check_sorted                           = true;
    // ILS
    mis_config.ils_iterations                         = 15000;
    mis_config.force_k                                = 1;
    mis_config.force_cand                             = 4;
    // Reductions
    mis_config.all_reductions                         = true;
    // Convergence
    mis_config.reduction_threshold                    = 350;
    mis_config.remove_fraction                        = 0.10;
    mis_config.extract_best_nodes                     = true;
    // Initial solution
    mis_config.start_greedy_adaptive = false;


    mis_log::instance()->set_config(mis_config);

    mis_log::instance()->set_graph(G);

    // Print setup information
    mis_log::instance()->print_graph();
    mis_log::instance()->print_config();

    std::cout << "Configuration done!" << std::endl;
    // Perform the online reduction algorithm
    online_ils online;
    online.perform_ils(mis_config, G, mis_config.ils_iterations);
    // Gather best solution
    std::vector<NodeID> independent_set(G.number_of_nodes(), false);
    forall_nodes(G, node) {
      independent_set[node] = G.getPartitionIndex(node);
      fG.setPartitionIndex(node, G.getPartitionIndex(node));
    } endfor

    mis_log::instance()->print_results_online();

    // transform <NodeID> vector to <bool> vector

    std::vector<bool> bool_independent_set(independent_set.size(), false);
    for(int i = 0; i < independent_set.size(); i++) {
        if(independent_set[i] == 1) {
            bool_independent_set[i] = true;
        }
    }

    return bool_independent_set;
}

std::vector<bool> createIndSet_REDUMIS(graph_access& G) {
    // configuration
    mis_log::instance()->restart_total_timer();
    mis_log::instance()->print_title();

    double timeLimit = (double)G.number_of_nodes()/500.0;
    std::cout << "Time Limit: " << timeLimit << std::endl;

    MISConfig mis_config;
    // copied from configuration_mis.cpp
    // Basic
    mis_config.population_size                        = 50;
    mis_config.repetitions                            = 50;
    mis_config.time_limit                             = timeLimit;
    // KaHIP
    mis_config.kahip_mode                             = 0;//FAST;
    // Randomization
    mis_config.seed                                   = 0;
    mis_config.diversify                              = true;
    mis_config.imbalance                              = 0.03;
    mis_config.randomize_imbalance                    = true;
    // Selection
    mis_config.enable_tournament_selection            = true;
    mis_config.tournament_size                        = 2;
    // Mutation
    mis_config.flip_coin                              = 1;
    // Combination
    mis_config.use_hopcroft                           = false;
    mis_config.optimize_candidates                    = true;
    // Multiway
    mis_config.use_multiway_vc                        = false;
    mis_config.multiway_blocks                        = 64;
    // Thresholds
    mis_config.insert_threshold                       = 150;
    mis_config.pool_threshold                         = 250;
    mis_config.pool_renewal_factor                    = 10.0;
    // Separator pool
    mis_config.number_of_separators                   = 10;
    mis_config.number_of_partitions                   = 10;
    mis_config.number_of_k_separators                 = 10;
    mis_config.number_of_k_partitions                 = 10;
    // Output
    mis_config.print_repetition                       = true;
    mis_config.print_population                       = false;
    mis_config.console_log                            = false;
    mis_config.check_sorted                           = true;
    // ILS
    mis_config.ils_iterations                         = 15000;
    mis_config.force_k                                = 1;
    mis_config.force_cand                             = 4;
    // Reductions
    mis_config.all_reductions                         = true;
    // Convergence
    mis_config.reduction_threshold                    = 350;
    mis_config.remove_fraction                        = 0.10;
    mis_config.extract_best_nodes                     = true;
    // Initial solution
    mis_config.start_greedy_adaptive = false;

    mis_log::instance()->set_config(mis_config);

    // "Read" the graph
    mis_log::instance()->set_graph(G);

    // Print setup information
    mis_log::instance()->print_graph();
    mis_log::instance()->print_config();

    if(mis_config.fullKernelization) {
    	//return run<branch_and_reduce_algorithm>(mis_config, G);
    }
    else {
	// Might be a bit confusingly named, but this is FastKer
    	//return run<full_reductions>(mis_config, G);
    }

    std::vector<bool> independent_set(G.number_of_nodes(), false);
    reduction_evolution<full_reductions> evo;
    //reduction_evolution<branch_and_reduce_algorithm> evo;
    std::vector<NodeID> best_nodes;
    evo.perform_mis_search(mis_config, G, independent_set, best_nodes);

    mis_log::instance()->print_results();
    if (mis_config.print_log) mis_log::instance()->write_log();
    int counter = 0;
    forall_nodes(G, node) {
            if( independent_set[node] ) {
                    counter++;
                    forall_out_edges(G, e, node) {
                            NodeID target = G.getEdgeTarget(e);
                            if(independent_set[target]) {
                                std::cout <<  "not an independent set!"  << std::endl;
                                exit(1);
                            }
                    } endfor
            }
    } endfor
    std::cout << "Independent set has size " << counter << std::endl;

    return independent_set;

}

/*


std::vector<bool> createIndSet_BRANCH_REDUCE(graph_access& G) {
    // Config
    mis_log::instance()->restart_total_timer();
    MISConfig mis_config;
    mis_log::instance()->set_config(mis_config);

    // Read graph
    assign_weights(G, mis_config);
    mis_log::instance()->set_graph(G);

    // Run Algorithm

    auto start = std::chrono::system_clock::now();

    branch_and_reduce_algorithm reducer(G, mis_config);
    reducer.run_branch_reduce();
    NodeWeight MWIS_weight = reducer.get_current_is_weight();

    auto end = std::chrono::system_clock::now();

    std::chrono::duration<float> branch_reduce_time = end - start;
    std::cout << "time " << branch_reduce_time.count() << "\n";
    std::cout << "MIS_weight " << MWIS_weight << "\n";

    reducer.apply_branch_reduce_solution(G);

    if (!is_IS(G)) {
            std::cerr << "ERROR: graph after inverse reduction is not independent" << std::endl;
            exit(1);
    } else {
            NodeWeight is_weight = 0;

            forall_nodes(G, node) {
                    if (G.getPartitionIndex(node) == 1) {
                            is_weight += G.getNodeWeight(node);
                    }
            } endfor

            std::cout << "MIS_weight_check " << is_weight << std::endl;
    }

    return
}

void assign_weights(graph_access& G, const MISConfig& mis_config) {
        constexpr NodeWeight MAX_WEIGHT = 200;

        if (mis_config.weight_source == MISConfig::Weight_Source::HYBRID) {
                forall_nodes(G, node) {
                        G.setNodeWeight(node, (node + 1) % MAX_WEIGHT + 1);
                } endfor
        } else if (mis_config.weight_source == MISConfig::Weight_Source::UNIFORM) {
                std::default_random_engine generator(mis_config.seed);
                std::uniform_int_distribution<NodeWeight> distribution(1,MAX_WEIGHT);

                forall_nodes(G, node) {
                        G.setNodeWeight(node, distribution(generator));
                } endfor
        } else if (mis_config.weight_source == MISConfig::Weight_Source::GEOMETRIC) {
                std::default_random_engine generator(mis_config.seed);
                std::binomial_distribution<int> distribution(MAX_WEIGHT / 2);

                forall_nodes(G, node) {
                        G.setNodeWeight(node, distribution(generator));
                } endfor
        }
}
*/
