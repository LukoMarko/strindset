#include "InverseMapping.hh"

InverseMapping::InverseMapping(unsigned size) {
    this->default_value = -1;
    this->map = std::vector<int>(size+1, this->default_value);
}

InverseMapping::InverseMapping(InverseMapping& object) {
    this->default_value = object.default_value;
    this->map = object.map;
    this->changes = object.changes;
}

InverseMapping::InverseMapping() {
    this->default_value = -1;
    this->map = std::vector<int>(0, this->default_value);
}

int InverseMapping::get(unsigned index) {
    return this->map[index]; // no reference! changes only with "set"
}

void InverseMapping::set(unsigned index, int value) {
    this->map[index] = value;
    this->changes.push(index);
}

void InverseMapping::clear() {
    unsigned temp_index;
    while(!this->changes.empty()) {
        temp_index = this->changes.top();
        this->changes.pop();
        this->map[temp_index] = this->default_value;
    }
}
