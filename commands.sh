#ToDo: Change the algorithm or multiple algorithms (just replace KaMISBufferedRedumis with Trivial Degree etc.)
# And change buffersize if the algorithm uses a Buffer

#After that save the file "commands.sh" and execute the bash script (./commands.sh")
#By doing this, the executable commands are saved in the "commands.txt" file

for path in `cat paths.txt`; do
for algo in Trivial Degree LubyDegree KaMISBufferedRedumis; do
for runs in 1; do
for maximize in 0; do
for buffersize in 1000;  do
for partMode in 0; do
        echo "./StrIndSet $path $algo $runs $maximize 0 0 $buffersize $partMode"
done
done
done
done
done
done > commands.txt

#POSSIBLE ALGORITHMS
#KaMISBufferedWeightedBrRedu KaMISBufferedWeightedLS KaMISBufferedOMis KaMISBufferedRedumis BufferedLubyDegree
#Trivial NodeWeight Degree WeightedDegree
#Basic RandomOffLine RandomDelete
#Luby LubyDegree

#DEBUG
#valgrind --tool=cachegrind
