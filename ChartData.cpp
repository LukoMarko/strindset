//


#include <sstream>
#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <map>

// represents the result of a specific algorithm on a specific graph
struct Result {

    // Value of a IndSet (weighted)
    double percentage;

    // time the algorithm took
    double duration;
};

// represents a graph
struct Graph {
    std::string name;
    std::string size;
};

struct Algorithm {
    std::string name;
    std::string buffersize;
    std::string partitionmode;
};

std::string getKeyword(std::string path);
std::string getFolder(std::string path);
std::string getFile(std::string path);
std::ifstream openFile(std::string filename);
double getPercentage(std::string result_line);
bool isFolder(std::string line, std::vector<std::string>& folders);
int isAlgo(std::string line, std::vector<Algorithm>& algorithms,std::string buffersize, std::string partitionmode);
std::string getAlgo(std::string line);
std::string getBuffersize(std::string line);
std::string getPartitionmode(std::string line);
double getDuration(std::string line);
std::string getGraphname(std::string line);
std::string getGraphSize(std::string line);
bool isKaMIS(std::string line, std::vector<std::string>& kaMIS);
void printChart(std::map<std::string, std::map<std::string, Result>>& chart, std::vector<Graph>& graphs, std::vector<Algorithm>& algorithms, std::string filename);



int main(int argn, char **argv) {
    std::string filename = "results.txt";
    std::cout << argn<< std::endl;
    if(argn == 2) {
        filename = argv[1];
    }
    std::cout << filename << std::endl;
    // represents the chart with graphs in the columns and algorithms in the lines
    std::map<std::string, std::map<std::string, Result>> chart;
    // saves all the foldernames that contain graphs
    std::vector<std::string> folders = {"citation", "cluster", "delaunay", "erg", "huge", "kron", "numeric", "rgg", "sparse", "street"};
    // saves all the graphs
    std::vector<Graph> graphs;
    // saves all the algorithms
    std::vector<Algorithm> algorithms;
    // all the KaMIS algorithms
    std::vector<std::string> kaMIS = {"online_mis", "redumis", "weighted_branch_reduce", "weighted_local_search"};

    // traverse through the results.txt file and complete the chart
    std::ifstream result_file(filename.c_str(), std::ifstream::in);
    if (!result_file) {
            std::cerr << "Error opening "<< filename << std::endl;
            exit(0);
    }
    std::string line;
    std::string currentGraph = "";
    while(std::getline(result_file, line)) {
        if(currentGraph == "") {
            if(isFolder(line, folders)) {
                // take the following line and add this to the graphs vector
                std::getline(result_file, line);
                std::cout << "new  graph " << line << std::endl;
                graphs.push_back({getGraphname(line),"0"});
                currentGraph = getGraphname(line);
                std::getline(result_file, line); // ignore the following empty line
                continue;
            }
        } else if(line == "") { // graph ended
            currentGraph = "";
            continue;
        } else {
            //std::cout << "line: " << line << std::endl;
            std::string buffersize = getBuffersize(line);
            std::string partitionmode = getPartitionmode(line);
            std::string og_line = line;
            std::string algo = getAlgo(line);
            if(isKaMIS(algo, kaMIS)) {
                algo = "Best KaMIS";
            }
            int index = isAlgo(algo, algorithms, buffersize, partitionmode);
            if(graphs[graphs.size()-1].size == "0") {
                graphs[graphs.size()-1].size = getGraphSize(line);
            }
            if(buffersize != "") {
                algorithms[index].buffersize = buffersize;
            }
            if(partitionmode != "") {
                algorithms[index].partitionmode = partitionmode;
            }

            double percentage = getPercentage(og_line);
            double duration = getDuration(og_line);

            if(percentage > chart[currentGraph][algo+buffersize+partitionmode].percentage || (percentage == chart[currentGraph][algo+buffersize+partitionmode].percentage && duration < chart[currentGraph][algo+buffersize+partitionmode].duration)) {
                std::cout << "Just added " << percentage << " and " << duration << " for the pair " << currentGraph << " - " << algo+buffersize+partitionmode << std::endl;
                chart[currentGraph][algo+buffersize+partitionmode] = {percentage, duration};
            }

            //ToDo verfollständige chart vector
            // ergänze Algorithms vector


        }
    }
    printChart(chart, graphs, algorithms, filename);
}

// checks if string represents a algorithm (also adds new ones) and returns index for algorithms vector
int isAlgo(std::string line, std::vector<Algorithm>& algorithms, std::string buffersize, std::string partitionmode) {
    int index = -1;
    for(int i = 0; i < algorithms.size(); i++) {
        if(line == algorithms[i].name && buffersize == algorithms[i].buffersize && partitionmode == algorithms[i].partitionmode) {
            std::cout << line << " == " << algorithms[i].name << std::endl;
            index = i;
            break;
        }
    }
    if(index == -1) {
        std::cout << "Added " << line << std::endl;
        algorithms.push_back({line, buffersize, partitionmode});
        index = algorithms.size()-1;
    }
    return index;
}

bool isFolder(std::string line, std::vector<std::string>& folders) {
    for(std::string folder : folders) {
        if(line == folder) {
            return true;
        }
    }
    return false;
}

// returns the algorithm name from a results.txt line
std::string getAlgo(std::string line) {
    std::string algorithm;
    for(int i = 0; i < line.size(); i++) {
        if(line[i] == ' ') {
            break;
        }
        algorithm.push_back(line[i]);
    }
    return algorithm;
}

// returns the Buffersize used for bufferedKaMIS
std::string getBuffersize(std::string line) {
    bool start = false;
    std::string buffersize = "";
    for(int i = 0; i < line.size(); i++) {
        if(line[i] == 'B' && line[i+1] == ':') {
            i++;
            start = true;
            continue;
        }
        if(start) {
            if(line[i] != ' ') {
                buffersize.push_back(line[i]);
            } else {
                break;
            }
        }
    }
    return buffersize;
}
// returns the PartitionMode used for bufferedKaMIS
std::string getPartitionmode(std::string line) {
    bool start = false;
    std::string PartitionMode = "";
    for(int i = 0; i < line.size(); i++) {
        if(line[i] == 'P' && line[i+1] == 'M' && line[i+2] == ':') {
            i++; // ignore "M"
            i++; // ignore ":"
            start = true;
            continue;
        }
        if(start) {
            if(line[i] != ' ') {
                PartitionMode.push_back(line[i]);
            } else {
                break;
            }
        }
    }
    return PartitionMode;
}

// returns the time duration the algorithm needed to find IndSet
double getDuration(std::string line) {
    bool start = false;
    int maxlenght = 7;
    std::string duration = "";
    for(int i = 0; i < line.size(); i++) {
        if(line[i-1] != 'h' && line[i] == 't' && line[i+1] == ':') {
            i++; // ignore ":"
            start = true;
            continue;
        }
        if(start) {
            if(line[i] != ' ' && duration.size() < maxlenght) {
                duration.push_back(line[i]);
            } else {
                break;
            }
        }
    }
    if(start == false) {
        duration = "9999999"; // 7 spaces
    }
    while(duration.size() < maxlenght-1) {
        duration.push_back('0');
    }

    return std::atof(duration.c_str());
}

std::string getGraphname(std::string line) {
    std::string graph;
    for(int i = 0; i < line.size(); i++) {
        if(line[i] == '-' && line[i+1] == 's' && line[i+2] == 'o') {
            break;
        }
        graph.push_back(line[i]);
    }
    return graph;
}

std::string getGraphSize(std::string line) {
    bool start = false;
    std::string size = "";
    for(int i = 0; i < line.size(); i++) {
        if(line[i] == '/') {
            start = true;
            continue;
        }
        if(start) {
            if(line[i] != ' ') {
                size.push_back(line[i]);
            } else {
                break;
            }
        }
    }
    return size;
}

bool isKaMIS(std::string line, std::vector<std::string>& kaMIS) {
    for(std::string algo : kaMIS) {
        if(algo == line) {
            return true;
        }
    }
    return false;
}

void printChart(std::map<std::string, std::map<std::string, Result>>& chart, std::vector<Graph>& graphs, std::vector<Algorithm>& algorithms, std::string filename) {
    std::ofstream output;
    std::string outputfilename = "chart_"+filename;
    output.open(outputfilename.c_str() , std::ios_base::app);

    for(Graph graph : graphs) {
        output << "Graph: " << graph.name << " " << graph.size << std::endl;
        for(Algorithm algo : algorithms) {
            output << "      " << chart[graph.name][algo.name+algo.buffersize+algo.partitionmode].percentage << "% " << chart[graph.name][algo.name+algo.buffersize+algo.partitionmode].duration << "m  " << algo.name << " " << algo.buffersize << " " << algo.partitionmode <<  std::endl;
        }
    }

    /*
    std::vector<unsigned> maxColumnSize;
    maxColumnSize.resize(graphs.size()+1);
    for(Algorithm algo : algorithms) {
        if(algo.name.size() > maxColumnSize[0]) {
            maxColumnSize[0] = algo.name.size();
        }
    }
    for(int i = 1 ; i < maxColumnSize.size(); i++) {
        for(Graph graph : graphs) {
            if(graph.name.size() > maxColumnSize[i]) {
                maxColumnSize[i] = graph.name.size();
            }
            if(graph.size.size() > maxColumnSize[i]) {
                maxColumnSize[i] = graph.size.size();
            }
            for(Algorithm algo : algorithms) {
                if(chart[graph.name][algo.name].duration.size() > maxColumnSize[i]) {
                    maxColumnSize[i] = chart[graph.name][algo.name].duration.size();
                }
            }

        }
    }
    // first line of the chart
    for(int line = 0; line < 2 ; i++) {

    }
    */
}
        /*
        if (!in) {
                std::cerr << "Error opening " << file << std::endl;
                continue;
        }
        std::vector<Result> resultsVector;
        std::string result_line;
        std::ofstream result_file;
        result_file.open("results.txt", std::ios_base::app);
        result_file << getKeyword(path) << std::endl;
        result_file << getFile(path) << std::endl << std::endl;
        while(std::getline(results, result_line)) {
            //std::cout << result_line << std::endl;
            double percentage = getPercentage(result_line);
            //std::cout << "percentage: " << getPercentage(result_line) << std::endl;
            resultsVector.push_back({result_line, percentage});
        }
        std::remove((file+".txt").c_str());
        std::sort(resultsVector.begin(), resultsVector.end(),[&](Result res1, Result res2) {
            return res1.percentage > res2.percentage;
        });
        for(Result line : resultsVector) {
            result_file << line.message << std::endl;
        }
        result_file << std::endl;

    }

}
*/

// Takes tha path to a graph and returns the "keyword" aka the category folder of a graph
std::string getKeyword(std::string path) {
    unsigned start;
    unsigned end = 0;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == '/') {
            if(end == 0) {
                end = i;
            } else {
                start = end;
                end = i;
            }
        }
    }
    std::string keyword;
    for(unsigned i = start+1; i < end; i++) {
        keyword.push_back(path[i]);
    }
    return keyword;
}

// Takes the path to a graph and returns the folder of the path
std::string getFolder(std::string path) {
    unsigned lastBackSlash;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == '/') {
            lastBackSlash = i;
        }
    }
    std::string folder;
    for(unsigned i = 0; i <= lastBackSlash; i++) {
        folder.push_back(path[i]);
    }
    return folder;
}

// Takes the path to a graph and returns the filename
std::string getFile(std::string path) {
    unsigned lastBackSlash;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == '/') {
            lastBackSlash = i;
        }
    }
    std::string file;
    for(unsigned i = lastBackSlash +1; i < path.size(); i++) {
            file.push_back(path[i]);
    }
    return file;
}

// Extracts the percentage a indSet has on nodeweight relative to the whole node weight
double getPercentage(std::string result_line) {
    unsigned lastBrace;
    unsigned lastPercentage;

    for(int i = 0; i < result_line.size(); i++) {
        if(result_line[i] == '(') {
            lastBrace = i;
        }
        if(result_line[i] == '%') {
            lastPercentage = i;
        }
    }
    std::string percentage;
    for(unsigned i = lastBrace +1; i < lastPercentage; i++) {
            percentage.push_back(result_line[i]);
    }
    return std::atof(percentage.c_str());
}
