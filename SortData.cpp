//


#include <sstream>
#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <algorithm>

std::string getKeyword(std::string path);
std::string getFolder(std::string path);
std::string getFile(std::string path);
std::ifstream openFile(std::string filename);
double getPercentage(std::string result_line);

struct Result {
    // ouput line of each Benchmark
    std::string message;
    // Value of a IndSet (weighted)
    double percentage;
};

int main(int argn, char **argv) {
    std::ifstream in("paths.txt", std::ifstream::in);
    if (!in) {
            std::cerr << "Error opening paths.txt" << std::endl;
            exit(0);
    }
    std::string path;
    while(std::getline(in, path)) {
        std::cout << path << std::endl;
        std::stringstream ss(path);
        std::string file = getFile(path);
        //std::cout << "folder " << getFolder(path) << std::endl;
        //std::cout << "file " << getFile(path) << std::endl;
        //std::cout << "keyword " << getKeyword(path) << std::endl;
        //std::cout << std::endl;
        std::ifstream results(file+".txt", std::ifstream::in);
        if (!in) {
                std::cerr << "Error opening " << file << std::endl;
                continue;
        }
        std::vector<Result> resultsVector;
        std::string result_line;
        std::ofstream result_file;
        result_file.open("results.txt", std::ios_base::app);
        result_file << getKeyword(path) << std::endl;
        result_file << getFile(path) << std::endl << std::endl;
        while(std::getline(results, result_line)) {
            //std::cout << result_line << std::endl;
            double percentage = getPercentage(result_line);
            //std::cout << "percentage: " << getPercentage(result_line) << std::endl;
            resultsVector.push_back({result_line, percentage});
        }
        std::remove((file+".txt").c_str());
        std::sort(resultsVector.begin(), resultsVector.end(),[&](Result res1, Result res2) {
            return res1.percentage > res2.percentage;
        });
        for(Result line : resultsVector) {
            result_file << line.message << std::endl;
        }
        result_file << std::endl;

    }

}

// Takes tha path to a graph and returns the "keyword" aka the category folder of a graph
std::string getKeyword(std::string path) {
    unsigned start;
    unsigned end = 0;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == '/') {
            if(end == 0) {
                end = i;
            } else {
                start = end;
                end = i;
            }
        }
    }
    std::string keyword;
    for(unsigned i = start+1; i < end; i++) {
        keyword.push_back(path[i]);
    }
    return keyword;
}

// Takes the path to a graph and returns the folder of the path
std::string getFolder(std::string path) {
    unsigned lastBackSlash;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == '/') {
            lastBackSlash = i;
        }
    }
    std::string folder;
    for(unsigned i = 0; i <= lastBackSlash; i++) {
        folder.push_back(path[i]);
    }
    return folder;
}

// Takes the path to a graph and returns the filename
std::string getFile(std::string path) {
    unsigned lastBackSlash;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == '/') {
            lastBackSlash = i;
        }
    }
    std::string file;
    for(unsigned i = lastBackSlash +1; i < path.size(); i++) {
            file.push_back(path[i]);
    }
    return file;
}

// Extracts the percentage a indSet has on nodeweight relative to the whole node weight
double getPercentage(std::string result_line) {
    unsigned lastBrace;
    unsigned lastPercentage;

    for(int i = 0; i < result_line.size(); i++) {
        if(result_line[i] == '(') {
            lastBrace = i;
        }
        if(result_line[i] == '%') {
            lastPercentage = i;
        }
    }
    std::string percentage;
    for(unsigned i = lastBrace +1; i < lastPercentage; i++) {
            percentage.push_back(result_line[i]);
    }
    return std::atof(percentage.c_str());
}
