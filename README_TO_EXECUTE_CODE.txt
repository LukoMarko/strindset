Hey

I reduced the amount of graphs to the graphs tested in my report (plus a couple of small ones). 
Before that all graphs together had a size of 20GB, now only ~300MB

STEP 1 (optional) (Changing tested graphs)
To change the graphs that should be tested, just remove/add lines from/to the "paths.txt" file.
By executing "./Paths.sh" the file "paths.txt" gets filled with all possible graphs (aka we reset all changes made to the paths.txt file)

STEP 2 (optional) (Changing used algorithms)
The script (commmands.sh) creates the executed commands, that are saved in "commands.txt"
A small Manual and a list of all possible commands is inside of the "commands.sh" file.

STEP 3 (Executing Commands)
To execute the commands, we use parallel. The command i always used looked like this:
cat commands.txt | parallel -j3 --progress
My pc only has 4 virtual cores, thats why i only use 3 to execute these commands
After executing, each graph has a .txt file in the folder, containing the results of all the algorithms that where tested on this graph

THE FOLLOWING STEPS HAVE TO BE EXECUTED IN THIS ORDER

STEP 4(OPTIONAL) (Add results from KaMIS)
just write "./add" in the terminal of the repo and the results of the unbuffered KaMIS algorithms are added to the specific file.
This is completely optional, but if you want to see the results of KaMIS, you have to execute this command before STEP 5

STEP 5 (Put all in one file)
To reduce the amount of files to one single file ("results.txt") just write "./sort" in the terminal.
This command reads in all result files and writes them into a single file, before it deletes all the individual result files.

STEP 6 (Optional) (better looking result table)
After creating the "results.txt" file, you can execute "./chart" to create a (sometimes) better looking table, than in the "results.txt" file. 
The file created is called "chart_results.txt".

First: Change Algorithms that 

