 #include <stack>
 #include <vector>

  // used to translate the normalized Subgraph-indices (1, 2, ..., Buffersize)
  // back to the original ones (arbitrary)
  class InverseMapping {
private:
    std::stack<unsigned> changes; // save every index of "map" that was changes from default -> faster "clear"
    std::vector<int> map; // map that translates normalized to original indices
    // int instead of
    int default_value;

public:
    InverseMapping();
    InverseMapping(unsigned size);
    InverseMapping(InverseMapping& object);
    int get(unsigned index);
    void set(unsigned index, int value);
    void clear();

};
