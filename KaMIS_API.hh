

// OMIS
#include "argtable3.h"
#include <stdio.h>
#include <string.h>
#include <iostream>

#include "data_structure/graph_access.h"
#include "data_structure/mis_permutation.h"
#include "graph_io.h"
#include "greedy_mis.h"
#include "ils/online_ils.h"
#include "ils/local_search.h"
#include "mis_config.h"
#include "mis_log.h"
//#include "app/parse_parameters_omis.h"
#include "timer.h"

// REDUMIS
#include "reduction_evolution.h"
#include "ils/ils.h"
#include "mis/kernel/ParFastKer/fast_reductions/src/full_reductions.h"

// BRANCH AND REDUCE
//#include "wmis/lib/mis/kernel/branch_and_reduce_algorithm.h"
//#include "branch_reduce.cpp"
//#include "app/parse_parameters.h"
//#include "wmis/lib/mis/mis_config.h"


std::vector<bool> createIndSet_OMIS(graph_access& G, graph_access& fG);
std::vector<bool> createIndSet_REDUMIS(graph_access& G);
//std::vector<bool> createIndSet_BRANCH_REDUCE(graph_access& G);
//void assign_weights(graph_access& G, const MISConfig& mis_config);
//std::vector<bool> createIndSet_LOCAL_SEARCH(graph_access& G);
