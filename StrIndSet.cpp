/******************************************************************************
 * Trivial Algorithm for Streaming Independent Set (Marko Lukosek)
 *****************************************************************************/

 #include <sstream>
 #include "graph_io.h"
 #include <unistd.h>
 #include <random>
 #include <chrono>
 #include <cstring>
 #include <iostream>
 #include <stdlib.h>
 #include <string.h>
 #include <algorithm>
 #include <map>
 #include "graph_access.h"

 #include "KaMIS_API.hh"
 #include "InverseMapping.hh"
 #include <unordered_set>


struct BufferNode {
    NodeID ID;
    NodeWeight nodeWeight; // only used by "center nodes"
    EdgeWeight edgeWeight; // only used by "future nodes" (aka non-center nodes)
};



 struct IndNode {
     bool in_IndSet;
     NodeWeight nodeWeight;
     NodeWeight degree;
     NodeWeight weightedDegree;
 };

 // global random number generator
 std::mt19937 rndm;

 void strIndSet(std::vector<IndNode>& IndSet, bool Save_to_G ,graph_access & G,const std::string & filename, bool debug, bool first_run, std::string decision, double p, std::vector<NodeID>& permutation, std::vector<bool>& marked, unsigned Buffersize = 0, unsigned PartitionMode = 0);
 void generateIndSet(const std::string & folder, const std::string & filename, std::string decision, unsigned runs, bool maximize = false, double p = 0.0, bool ReadGraph = false, unsigned Buffersize = 0, unsigned PartitionMode = 0);
 void generateLogFile(const std::string & filename, std::string decision, unsigned runs, std::string IndSetCheck);
 void decide_IndSet(std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, bool debug, bool (*decide)(IndNode, IndNode));
 void check_IndSet(std::vector<IndNode>& IndSet, graph_access& G, bool debug, std::ofstream& output);

 bool decide_Trivial(IndNode a, IndNode b);
 bool decide_NodeWeight(IndNode a, IndNode b);
 bool decide_Degree(IndNode a, IndNode b);
 bool decide_WeightedDegree(IndNode a, IndNode b);

 void generateSubSet(unsigned size, double p, std::vector<bool>& SubSet);

 void invertIndSet(std::vector<IndNode>& IndSet);
 void decide_Basic(std::vector<IndNode>& IndSet, std::vector<bool>& SubSet, std::vector<NodeID>& neighbors, NodeID node, bool debug);

 void generatePermutation(unsigned size, std::vector<NodeID>& permutation);
 void decide_RandomOffLine(std::vector<IndNode>& IndSet, std::vector<NodeID>& permutation, std::vector<NodeID>& neighbors, NodeID node, bool debug);

 void decide_RandomDelete(std::vector<IndNode>& IndSet, std::vector<NodeID>&  neighbors, NodeID node,bool debug);
 std::vector<std::string> splitPath(std::string Path);


 void decide_Lubys(std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, std::vector<bool>& marked, std::vector<NodeID>& permutation);

 void decide_LubyDegree(std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, std::vector<bool>& marked);

 bool isMaxml(std::vector<IndNode>& IndSet, graph_access& G);

 bool isMaxml(std::vector<bool>& IndSet, graph_access& G);

 void generateMetisSubgraph(std::vector<std::vector<BufferNode>>& Buffer, unsigned Buffersize, std::string filename, std::string decision, int ew, std::vector<IndNode>& IndSet, std::vector<bool>& marked, InverseMapping& inverse, InverseMapping& normalize);

 void generateSubgraph(std::vector<std::vector<BufferNode>>& Buffer, unsigned Buffersize, std::string decision, int ew, std::vector<IndNode>& IndSet, std::vector<bool>& marked, InverseMapping& inverse, InverseMapping& normalize);

 unsigned countBufferEdges(std::vector<std::vector<BufferNode>>& Buffer);

 std::string bufferedSubGraphName(std::string filename, std::string decision, unsigned Buffersize);
 std::string bufferedIndSetName(std::string filename, std::string decision, unsigned Buffersize);

 graph_access genereteSubgraphAccess(std::vector<std::vector<BufferNode>>& Buffer);

 std::vector<bool> BufferedLubyDegree(graph_access& G);
 void createValidGraph(std::vector<std::vector<BufferNode>>& Buffer, InverseMapping& inverse);
 void createValidGraph(std::vector<std::vector<BufferNode>>& Buffer, InverseMapping& inverse,  InverseMapping& normalize);

 void checkBufferedGraph(std::vector<std::vector<BufferNode>>& Buffer, std::string subGraphName, int weights);


 int main(int argn, char **argv)
 {

     rndm = std::mt19937(time(NULL));
     std::cout << "argn: " << argn << std::endl;
     if( argn != 5 && argn != 6 && argn != 7 && argn != 8 && argn != 9) {
             std::cout <<  "Usage: StrIndSet PATH DECISION #RUNS MAXIMIZE "  << std::endl;
             std::cout <<  "OR:    StrIndSet PATH DECISION #RUNS MAXIMIZE P"  << std::endl;
             std::cout <<  "OR:    StrIndSet PATH DECISION #RUNS MAXIMIZE P ReadGraph" << std::endl;
             std::cout <<  "OR:    StrIndSet PATH DECISION #RUNS MAXIMIZE P ReadGraph Buffersize" << std::endl;
             std::cout <<  "OR:    StrIndSet PATH DECISION #RUNS MAXIMIZE P ReadGraph Buffersize PartitionMode" << std::endl;
             exit(0);
     }


     std::string PATH = argv[1];
     std::vector<std::string> path = splitPath(PATH);
     std::string folder = path[0];
     std::string filename = path[1];
     std::string decision = argv[2];
     int runs = atoi(argv[3]);
     bool maximize = std::stoi(argv[4]);
     double p = 0.0;
     if(argn >= 6) {
         p = std::strtod(argv[5], nullptr);
     }
     bool ReadGraph = false;
     if(argn >= 7) {
         ReadGraph = std::stoi(argv[6]);
     }
     unsigned Buffersize = 0;
     if(argn >= 8) {
         Buffersize = atoi(argv[7]);
     }
     unsigned PartitionMode = 0;
     if(argn >= 9) {
         PartitionMode = atoi(argv[8]);
     }

     std::cout << "folder " << folder << std::endl;
     std::cout << "file " << filename << std::endl;
     std::cout << "decision: " << decision << std::endl;
     std::cout << "runs " << runs << std::endl;
     std::cout << "maximize: " << maximize << std::endl;
     std::cout << "p: " << p << std::endl;
     std::cout << "ReadGraph: " << ReadGraph << std::endl;
     std::cout << "Buffersize: " << Buffersize << std::endl;
     std::cout << "PartitionMode: " << PartitionMode << std::endl;

     generateIndSet(folder, filename, decision, runs, maximize, p, ReadGraph, Buffersize, PartitionMode);
 }
// reads in the graph and finds Streaming independent set
// if Save_to_G is true, it also saves the graph information
// in a graph_access file (to test other (non-streaming) algorithms)
// Occurrence of graph_access calls: l78, l88, l105, l121, l144
void strIndSet(std::vector<IndNode>& IndSet, bool Save_to_G, graph_access & G,
    const std::string & filename, bool debug, bool first_run, std::string decision, double p, std::vector<NodeID>& permutation, std::vector<bool>& marked, unsigned Buffersize, unsigned PartitionMode) {
    std::string line;


    //used for the basic Algorithm
    std::vector<bool> SubSetX;

    // only used for the RandomDelete algorithm
    bool first = true;


    // open file for reading
    std::ifstream in(filename.c_str());
    if (!in) {
            std::cerr << "Error opening " << filename << std::endl;
            exit(0);
    }

    long nmbNodes;
    long nmbEdges;

    std::getline(in,line);
    //skip comments
    while( line[0] == '%' ) {
            std::getline(in, line);
    }

    int ew = 0;
    std::stringstream ss(line);
    // read in first three numbers
    // number of nodes, number of edges, graph conf. parameter
    ss >> nmbNodes;
    ss >> nmbEdges;
    ss >> ew;

    if( 2*nmbEdges > std::numeric_limits<int>::max() || nmbNodes > std::numeric_limits<int>::max()) {
            std::cerr <<  "The graph is too large. Currently only 32bit supported!"  << std::endl;
            exit(0);
    }

    if(first_run) {
        IndSet.resize(nmbNodes);
    }

    if(marked.empty()) {
        marked.resize(nmbNodes);
    }

    bool read_ew = false;
    bool read_nw = false;
    // check graph conf. parameter
    if(ew == 1) {
            read_ew = true; //graph has (only) edge weights
    } else if (ew == 11) {
            read_ew = true; // graph has edge weights
            read_nw = true; // and node weights
    } else if (ew == 10) {
            read_nw = true; // graph has (only) node weights
    }
    nmbEdges *= 2; //since we have forward and backward edges

    NodeID node_counter   = 0;
    EdgeID edge_counter   = 0;
    long long total_nodeweight = 0;
    NodeID node_G;
    if(Save_to_G) {
            G.start_construction(nmbNodes, nmbEdges);
            std::cout << "Size set to: " << nmbNodes << " nodes and " << nmbEdges << " edges " << std::endl;
    }

    if(nmbNodes < Buffersize) {
        Buffersize = nmbNodes;
    }

    // used for Buffered KaMIS
    // in other words: Buffer saves the later created "neighbor" vectors
    // but with the addition of the current node, pushed to the front of "neighbors"
    std::vector<std::vector<BufferNode>> Buffer;
    InverseMapping inverse;
    InverseMapping normalize;
    if(decision == "KaMISBufferedRedumis" || decision == "KaMISBufferedOMis" || decision == "KaMISBufferedWeightedBrRedu" || decision == "KaMISBufferedWeightedLS" || decision == "BufferedLubyDegree") {
            Buffer.reserve(Buffersize);
            inverse = InverseMapping((unsigned)Buffersize);
            normalize = InverseMapping((unsigned)nmbNodes);
    }


    while(  std::getline(in, line)) {

            if (line[0] == '%') { // a comment in the file
                    continue;
            }
            //std::cout << line << std::endl;

            NodeID node = node_counter++;

            std::stringstream ss(line);


            NodeWeight weight = 1;
            if( read_nw ) {
                    ss >> weight;
                    total_nodeweight += weight;
                    if( total_nodeweight > (long long) std::numeric_limits<NodeWeight>::max()) {
                            std::cerr <<  "The sum of the node weights is too large (it exceeds the node weight type)."  << std::endl;
                            std::cerr <<  "Currently not supported. Please scale your node weights."  << std::endl;
                            exit(0);
                    }
            }
            if(first_run) {
                IndSet[node].nodeWeight = weight;
            }
            if(Save_to_G) {
                node_G = G.new_node();
                G.setPartitionIndex(node_G, 0);
                G.setNodeWeight(node_G, weight);
            }

            // since we dont want to rely on the graph_access object
            // we have to save the neighbors of a node separately

            // it only saves the neighbors for the current node and
            // is only used for decide_IndSet()
            std::vector<NodeID> neighbors;
            std::vector<BufferNode> bufferNeighbors; // only used for buffered KaMIS (to not rely on graph access object)
            if(decision == "KaMISBufferedRedumis" || decision == "KaMISBufferedOMis" || decision == "KaMISBufferedWeightedBrRedu" || decision == "KaMISBufferedWeightedLS" || decision == "BufferedLubyDegree") {
                bufferNeighbors.push_back({node, weight, 0}); // add current "center node" into the buffer
            }

            NodeID target;
            while( ss >> target ) {
                    //check for self-loops
                    if(target-1 == node) {
                            std::cerr <<  "The graph file contains self-loops. This is not supported. Please remove them from the file."  << std::endl;
                    }
                    neighbors.push_back(target-1);

                    EdgeWeight edge_weight = 1;
                    if( read_ew ) {
                            ss >> edge_weight;
                    }
                    edge_counter++;
                    if(first_run) {
                        IndSet[node].degree += 1;
                        IndSet[node].weightedDegree += edge_weight;
                    }
                    if(Save_to_G) {
                        EdgeID e = G.new_edge(node_G, target-1);
                        G.setEdgeWeight(e, edge_weight);
                    }
                    if(decision == "KaMISBufferedRedumis" || decision == "KaMISBufferedOMis" || decision == "KaMISBufferedWeightedBrRedu" || decision == "KaMISBufferedWeightedLS" || decision == "BufferedLubyDegree") {
                        bufferNeighbors.push_back({target-1, 0, edge_weight}); // add neighbor into the buffer
                    }
            }

            if(decision == "NodeWeight") {
                decide_IndSet(IndSet, neighbors, node, debug, &decide_NodeWeight);
            } else if(decision == "Degree") {
                decide_IndSet(IndSet, neighbors, node, debug, &decide_Degree);
            } else if(decision == "WeightedDegree") {
                decide_IndSet(IndSet, neighbors, node, debug, &decide_WeightedDegree);
            } else if(decision == "Trivial") {
                decide_IndSet(IndSet, neighbors, node, debug, &decide_Trivial);
            } else if(decision == "Basic") {
                if(SubSetX.empty()) {
                    generateSubSet(nmbNodes, p, SubSetX);
                    invertIndSet(IndSet);
                }
                decide_Basic(IndSet, SubSetX, neighbors, node, debug);
            }
            else if(decision == "RandomOffLine") {
                if(permutation.empty()) {
                    generatePermutation(nmbNodes, permutation);
                    invertIndSet(IndSet);
                }
                decide_RandomOffLine(IndSet, permutation, neighbors, node, debug);
            } else if(decision == "RandomDelete") {
                if(first) {
                    invertIndSet(IndSet);
                    first = false;
                }
                decide_RandomDelete(IndSet, neighbors, node, debug);
            } else if(decision == "ReadGraph"){
                // no decision yet (used to read Graph (aka node degree)
                // to calculate maxP before calculating IndSet
            } else if(decision == "Luby"){
                if(permutation.empty()) {
                    generatePermutation(nmbNodes, permutation);
                }
                if(!marked[node]) {
                    //std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, std::vector<bool>& marked, std::vector<NodeID>& permutation) {
                    decide_Lubys(IndSet, neighbors, node, marked, permutation);
                }
            } else if(decision == "KaMISBufferedRedumis" || decision == "KaMISBufferedOMis" || decision == "KaMISBufferedWeightedBrRedu" || decision == "KaMISBufferedWeightedLS" || decision == "BufferedLubyDegree") {
                if(!marked[bufferNeighbors[0].ID]) {
                    Buffer.push_back(bufferNeighbors);
                    /*
                    if(Buffersize >= 100) {
                        if(Buffer.size()%(Buffersize/100) == 0) {
                            //std::cout << Buffer.size() << std::endl;
                            std::cout << ((float)(Buffer.size())*100.0)/(float)(Buffersize)<< "%" << std::endl;
                        }
                    }
                    */
                }

                if(Buffer.size() == Buffersize || in.peek()==EOF) {
                    /*
                    std::cout << "LETS GO!!!" << std::endl;
                    if(in.peek() == EOF) {
                        std::cout << "All nodes contained" << std::endl;
                    } else {
                        std::cout << "Some nodes left" << std::endl;
                    }
                    */
                    //generateSubgraph(Buffer, Buffersize, decision, ew, IndSet, marked, inverse, normalize);
                    generateMetisSubgraph(Buffer, Buffersize, filename, decision, ew, IndSet, marked, inverse, normalize);
                    Buffer.clear();
                    //break;
                }

            } else if(decision == "LubyDegree") {
                if(!marked[node]) {
                    //std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, std::vector<bool>& marked, std::vector<NodeID>& permutation) {
                    decide_LubyDegree(IndSet, neighbors, node, marked);
                }
            } else {
                std::cerr << "Decision Type unknown!" << std::endl;
                exit(0);
            }

            if(in.peek()==EOF) {//.eof()) {
                break;
            }
    }
    // deletes the file used by BufferedKaMIS
    if(decision == "KaMISBufferedRedumis" || decision == "KaMISBufferedOMis" || decision == "KaMISBufferedWeightedBrRedu" || decision == "KaMISBufferedWeightedLS" || decision == "BufferedLubyDegree") {
        // no removing needed, since we dont use the harddrive anymore
        //std::cout << "Files deleted!" << std::endl;
        remove(bufferedSubGraphName(filename, decision, Buffersize).c_str());
        remove(bufferedIndSetName(filename, decision, Buffersize).c_str());
    }


    if( edge_counter != (EdgeID) nmbEdges ) {
            std::cerr <<  "number of specified edges mismatch"  << std::endl;
            std::cerr <<  edge_counter <<  " " <<  nmbEdges  << std::endl;
            exit(0);
    }

    if( node_counter != (NodeID) nmbNodes) {
            std::cerr <<  "number of specified nodes mismatch"  << std::endl;
            std::cerr <<  node_counter <<  " " <<  nmbNodes  << std::endl;
            exit(0);
    }

    if(Save_to_G) {
        G.finish_construction();
    }
}

// used to make a Trivial decision between two nodes
bool decide_Trivial(IndNode a, IndNode b) {
    return true;
}
// used to make a decision based on Node Weights
bool decide_NodeWeight(IndNode a, IndNode b) {
    return a.nodeWeight >= b.nodeWeight;
}

//used to make a decision based on Node Degree
bool decide_Degree(IndNode a, IndNode b) {
    return a.degree <= b.degree;
}
// used to make a decision based on Weighted Degree
bool decide_WeightedDegree(IndNode a, IndNode b) {
    return a.weightedDegree <= b.weightedDegree;
}


void decide_Lubys(std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, std::vector<bool>& marked, std::vector<NodeID>& permutation) {
    // the current node cant be in the IndSet and neither can any of its neigbors
    // but some neighbors can already be marked
    bool move_node = true;
    // check if current node has smaller permutation index than all its unmarked neighbors
    for(NodeID neighbor : neighbors) {
        if(!marked[neighbor]) {
            if(permutation[neighbor] < permutation[node]) {
                move_node = false;
                break;
            }
        }
    }
    // if so, add current node to IndSet and mark the node and all its neigbors
    if(move_node) {
        IndSet[node].in_IndSet = true;
        marked[node] = true;
        for(NodeID neighbor : neighbors) {
            marked[neighbor] = true;
        }
    }
}

void decide_LubyDegree(std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, std::vector<bool>& marked) {
    // the current node cant be in the IndSet and neither can any of its neigbors
    // but some neighbors can already be marked
    bool move_node = true;
    // check if current node has smaller degree than any of its neighbors
    for(NodeID neighbor : neighbors) {
        if(!marked[neighbor]) {
            if(IndSet[neighbor].degree < IndSet[node].degree) {
                move_node = false;
                break;
            }
        }
    }
    // if so, add current node to IndSet and mark the node and all its neigbors
    if(move_node) {
        IndSet[node].in_IndSet = true;
        marked[node] = true;
        for(NodeID neighbor : neighbors) {
            marked[neighbor] = true;
        }
    }
}


// called after reading in a new Node to decide if the node
// should be moved to the ind set or not, based on its node weight
void decide_IndSet(std::vector<IndNode>& IndSet, std::vector<NodeID>& neighbors, NodeID node, bool debug, bool (*decide)(IndNode, IndNode)) {
    // Check if node already in the Ind. Set
    if(!IndSet[node].in_IndSet) {
        bool movable = true;
        for(NodeID neighbor : neighbors){
            // A neighbor is already in the IndSet
            if(IndSet[neighbor].in_IndSet) {
                // Based of a decision defined in other functions
                // we either move the node to Ind Ser or not
                if(decide(IndSet[neighbor],IndSet[node])) {
                    movable = false;
                    if(debug) { std::cout << "Node " << node << " not moved because of " << neighbor << std::endl;}
                    break;
                }
                // otherwise movable stays true and we move the node in the IndSet
            }
        }
        if(movable) {
            if(debug) { std::cout << "Node " << node << " added to IndSet" << std::endl; }
            IndSet[node].in_IndSet = true;
            //remove all neighbors from the IndSet
            for(NodeID neighbor : neighbors){
                if(IndSet[neighbor].in_IndSet) {
                    if(debug) { std::cout << "Node " << neighbor << " removed because of " << node << std::endl; }
                    IndSet[neighbor].in_IndSet = false;
                }
            }
        }
    }
}

// called to check if the Set is independent and maximal and to print some information (if debug enabled)
void check_IndSet(std::vector<IndNode>& IndSet, graph_access& G, bool debug, std::ofstream& output) {
    // first check the ind set
    NodeID index = 0;
    bool is_maximal = true;
    bool is_independent = true;

    NodeWeight weight_sum = 0;
    NodeWeight total_weight_sum = 0;
    NodeID number_of_nodes = 0;
    NodeID IndSet_neighbor;
    for(IndNode node : IndSet) {
        // check if Ind Set is maximal
        // (every node that is not in the ind set has at least one
        // neighbor in the ind set)
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // check if Ind Set is independent
        // (Every node in the Ind Set has no neighbor that is also
        // in the Ind Set)
        bool has_IndSet_neighbor = false;
        forall_out_edges(G,e,index) {
            if(G.getEdgeTarget(e) != index) {
                if(IndSet[G.getEdgeTarget(e)].in_IndSet) {
                    has_IndSet_neighbor = true;
                    IndSet_neighbor = G.getEdgeTarget(e);
                    break;
                }
            }
        }endfor
        if(!has_IndSet_neighbor && !node.in_IndSet) {
            if(debug) { std::cout << "The node " << index << " could fit into the independent set!" << std::endl; }
            is_maximal = false;
        }
        if(has_IndSet_neighbor && node.in_IndSet) {
            if(debug) { std::cout << "The node " << IndSet_neighbor << " and its neighbor " << index << " are both in the independent set!" << std::endl;}
            is_independent = false;
        }

        // create information of the Ind set
        total_weight_sum += G.getNodeWeight(index);
        if(node.in_IndSet) {
            number_of_nodes++;
            weight_sum += G.getNodeWeight(index);
        }
        index++;
    }

    output << "Max:" << is_maximal << " Ind:" << is_independent;
    output << " Nds:" << number_of_nodes << "/" << index << " (" << ((float)(number_of_nodes)*100.0)/(float)(index) << "%)";
    output << " Wght:" << weight_sum << "/" << total_weight_sum << " (" << ((float)(weight_sum)*100.0)/(float)(total_weight_sum) << "%)";
}

void generateIndSet(const std::string & folder, const std::string & filename, std::string decision, unsigned runs, bool maximize, double p, bool ReadGraph, unsigned Buffersize, unsigned PartitionMode) {
    graph_access G;
    std::vector<IndNode> IndSet;

    std::ofstream outfile;
    outfile.open(filename+".txt", std::ios_base::app);
    //Enables debug information for the following functions
    bool debug = false;

    // used by RandomOffLine and Luby's algorithm
    std::vector<NodeID> permutation;

    // only used for the Lubys algorithm
    std::vector<bool> marked;


    if(ReadGraph) {
        strIndSet(IndSet, false, G, folder+filename, debug, true, "ReadGraph", p, permutation, marked);
        //p = calculateMaxP(IndSet);
        //std::cout << "p is "<< p << std::endl;
    }

    std::chrono::duration<double> delta;

    for(int i = 1; i <= runs; i++) {
        auto start_decide = std::chrono::steady_clock::now();
        if(i == 1) {
            strIndSet(IndSet, true, G, folder+filename, debug, true, decision, p, permutation, marked, Buffersize, PartitionMode);
            while((decision == "Luby" || decision == "LubyDegree") && !isMaxml(IndSet, G)) {
                //std::cout << "r: " << runs << std::endl;
                //runs++;
                strIndSet(IndSet, false, G, folder+filename, debug, false, decision, p, permutation, marked, Buffersize, PartitionMode);
            }
        } else {
            strIndSet(IndSet, false, G, folder+filename, debug, false, decision, p, permutation, marked, Buffersize, PartitionMode);
        }
        /*
        forall_nodes(G, node) {
            std::cout << node << " ";
            forall_out_edges(G, edge, node) {
                std::cout << G.getEdgeTarget(edge) << " ";
            }endfor
            std::cout << std::endl;
        }endfor
        */
        auto stop_decide = std::chrono::steady_clock::now();

        delta = std::chrono::duration_cast<std::chrono::duration<double>>(stop_decide - start_decide);
        //delta = delta/60.0;
        if(ReadGraph) {
            outfile << "RG:";
        }
        outfile << decision << " " << i << " ";
        if(p != 0.0) {
            outfile << p << " ";
        }
        if(decision == "KaMISBufferedRedumis" || decision == "KaMISBufferedOMis" || decision == "KaMISBufferedWeightedBrRedu" || decision == "KaMISBufferedWeightedLS" || decision == "BufferedLubyDegree") {
            outfile << "B:" << Buffersize << " ";
            outfile << "PM:" << PartitionMode << " ";
        }
        auto start_check = std::chrono::high_resolution_clock::now();
        check_IndSet(IndSet, G, false, outfile);
        auto stop_check = std::chrono::high_resolution_clock::now();
        outfile << " Δt:" << delta.count() << " ";
        //duration = std::chrono::duration_cast<std::chrono::microseconds>(stop_check - start_check);
        //std::cout << "check:  " << duration.count() << std::endl;
        outfile << std::endl;
    }
    if(maximize) {
        auto start_maximize = std::chrono::steady_clock::now();
        strIndSet(IndSet, false, G, folder+filename, debug, false, "Trivial", p, permutation, marked);
        auto stop_maximize = std::chrono::steady_clock::now();
        std::chrono::duration<double> delta_maximize = std::chrono::duration_cast<std::chrono::duration<double>>(stop_maximize - start_maximize);
        outfile << decision << " " << "Mxml" << " ";
        check_IndSet(IndSet, G, false, outfile);
        delta += delta_maximize;
        outfile << " Δt:" << delta.count() << " ";
        outfile << std::endl;
    }
    outfile.close();
}

// used to set "in_IndSet" for every node to "true" since some
// algorithms dont generate the IndSet but V/IndSet
 void invertIndSet(std::vector<IndNode>& IndSet) {
     for(IndNode& node : IndSet) {
         node.in_IndSet = true;
     }
 }

 // generates a SubSet needed for the Basic Algorithm
 // it contains (Nodes in Graph)*p elements
  void generateSubSet(unsigned size, double p, std::vector<bool>& SubSet) {
      SubSet.resize(size);
      unsigned SubSetSize = (unsigned)size*p;
      //std::cout << SubSetSize << " = " << size << " * " << p << std::endl;
      unsigned newNode;
      for(int i = 0; i < SubSetSize; i++) {
          newNode = rndm()%size;
          while(SubSet[newNode]) {
              newNode = rndm()%size;
          }
          //std::cout << i << "  - " << newNode << " is in the SubSet" << std::endl;
          SubSet[newNode] = true;
      }
  }

// generates a random permutation for the RandomOffLine algorithm
 void generatePermutation(unsigned size, std::vector<NodeID>& permutation) {
     permutation.reserve(size);
     for(int i = 0; i < size; i++) {
         //at first every position points to itself
         permutation.push_back(i);
     }
     unsigned position;
     unsigned temp;
     for(int i = 0; i < size; i++) {
         // find a new position from the remaining ones
         position = (rndm()%(size-i))+i;

         // and swap with position i
         temp = permutation[i];
         permutation[i] = permutation[position];
         permutation[position] = temp;
     }
     std::vector<bool> check;
     check.resize(size);
     for(int i = 0; i < size; i++) {
         if(check[permutation[i]]) {
             std::cerr << "Permutation is conflicting!" << std::endl;
         }
         check[permutation[i]] = true;
     }
 }

 // implementation of the Basic Algorithm
  void decide_Basic(std::vector<IndNode>& IndSet, std::vector<bool>& SubSet, std::vector<NodeID>& neighbors, NodeID node, bool debug) {
       for(NodeID neighbor : neighbors) {
           // to only take a edge once
           if(node < neighbor) {
               // If the Edge e = {node, neighbor} is in the SubSet or completely outside
               if((SubSet[node] && SubSet[neighbor]) || (!SubSet[node] && !SubSet[neighbor])) {
                   // Take one of both nodes
                   if(rndm()%2 == 0) {
                       // And add it into S (aka remove it from V, since I = V/S)
                       //std::cout << node << " got kicked out of IndSet" << std::endl;
                       IndSet[node].in_IndSet = false;
                   } else {
                       IndSet[neighbor].in_IndSet = false;
                   }
               } else {// If at least one node is not in the SubSet
                   // take a node that is not in the Sub Set, and add it to S
                   if(!SubSet[neighbor]) {
                       //std::cout << neighbor << " got kicked out of IndSet" << std::endl;
                       IndSet[neighbor].in_IndSet = false;
                   }
                   if(!SubSet[node]) {
                       //std::cout << node << " got kicked out of IndSet" << std::endl;
                       IndSet[node].in_IndSet = false;
                   }
               }
           }
       }
  }

// Implementation of the RandomOffLine algorithm
 void decide_RandomOffLine(std::vector<IndNode>& IndSet, std::vector<NodeID>& permutation, std::vector<NodeID>& neighbors, NodeID node, bool debug) {
     for(NodeID neighbor : neighbors) {
         // to only take a edge = {a,b} once
         if(node < neighbor) {
             if(permutation[node] > permutation[neighbor]) {
                 // Take either pi(a) or pi(b) into S (aka remove it from V, since I = V/S)
                 IndSet[node].in_IndSet = false;
             } else {
                 IndSet[neighbor].in_IndSet = false;
             }
         }
     }
 }

  void decide_RandomDelete(std::vector<IndNode>& IndSet, std::vector<NodeID>&  neighbors, NodeID node,bool debug) {
      for(NodeID neighbor : neighbors) {
          // to only take a edge = {a,b} once
          if(node < neighbor) {
              if(IndSet[node].in_IndSet && IndSet[neighbor].in_IndSet) {

                  if(rndm()%2) {
                      // Take either a or b into S (aka remove it from V, since I = V/S)
                      IndSet[node].in_IndSet = false;
                  } else {
                      IndSet[neighbor].in_IndSet = false;
                  }
              }
          }
      }
  }


  std::vector<std::string> splitPath(std::string Path) {
      unsigned lastBackSlash;
      for(int i = 0; i < Path.size(); i++) {
          if(Path[i] == '/') {
              lastBackSlash = i;
          }
      }
      std::string folder;
      std::string file;
      for(unsigned i = 0; i < Path.size(); i++) {
          if(i <= lastBackSlash) {
              folder.push_back(Path[i]);
          }else {
              file.push_back(Path[i]);
          }

      }
      std::vector<std::string> result;
      result.push_back(folder);
      result.push_back(file);
      return result;
  }

// check if a IndSet is already maximal (used for Lubys algorithm)
  bool isMaxml(std::vector<IndNode>& IndSet, graph_access& G) {
      NodeID index = 0;
      bool is_maximal = true;

      NodeID IndSet_neighbor;

      for(IndNode node : IndSet) {
          // check if Ind Set is maximal
          // (every node that is not in the ind set has at least one
          // neighbor in the ind set)
          bool has_IndSet_neighbor = false;
          forall_out_edges(G,e,index) {
              if(G.getEdgeTarget(e) != index) {
                  if(IndSet[G.getEdgeTarget(e)].in_IndSet) {
                      has_IndSet_neighbor = true;
                      IndSet_neighbor = G.getEdgeTarget(e);
                      break;
                  }
              }
          }endfor
          if(!has_IndSet_neighbor && !node.in_IndSet) {
              is_maximal = false;
              break;
          }
          index++;
      }
      return is_maximal;

  }

  // check if a IndSet is already maximal (used for Lubys algorithm)
    bool isMaxml(std::vector<bool>& IndSet, graph_access& G) {
        NodeID index = 0;
        bool is_maximal = true;

        NodeID IndSet_neighbor;

        for(bool node : IndSet) {
            // check if Ind Set is maximal
            // (every node that is not in the ind set has at least one
            // neighbor in the ind set)
            bool has_IndSet_neighbor = false;
            forall_out_edges(G,e,index) {
                if(G.getEdgeTarget(e) != index) {
                    if(IndSet[G.getEdgeTarget(e)]) {
                        has_IndSet_neighbor = true;
                        IndSet_neighbor = G.getEdgeTarget(e);
                        break;
                    }
                }
            }endfor
            if(!has_IndSet_neighbor && !node) {
                is_maximal = false;
                break;
            }
            index++;
        }
        return is_maximal;

    }
// used for buffered KaMIS to generate a Subgraph as a Textfile
  void generateMetisSubgraph(std::vector<std::vector<BufferNode>>& Buffer, unsigned Buffersize, std::string filename, std::string decision, int ew, std::vector<IndNode>& IndSet, std::vector<bool>& marked, InverseMapping& inverse, InverseMapping& normalize) {
      std::vector<std::vector<BufferNode>> Memory = Buffer; // to know later which neighbors to mark (center node in IndSet)

      createValidGraph(Buffer, inverse, normalize);

      std::ofstream subGraph;
      std::ifstream KaMISIndSet;

      std::string subGraphName = bufferedSubGraphName(filename, decision, Buffersize);
      std::string IndSetName = bufferedIndSetName(filename, decision, Buffersize);

      // empty the file from last run
      subGraph.open(subGraphName, std::ios::out | std::ios::trunc );
      subGraph.close();
      subGraph.open(subGraphName, std::ios_base::app);

      KaMISIndSet.open(IndSetName.c_str(), std::ios::out | std::ios::trunc);
      KaMISIndSet.close();

      unsigned edges = countBufferEdges(Buffer);
      subGraph << Buffer.size() << " " << edges << " " << ew << std::endl;
      for(std::vector<BufferNode> neighbors : Buffer) {
          for(int i = 0; i < neighbors.size(); i++) {
              BufferNode node = neighbors[i];
              if(i > 0) {
                  subGraph << node.ID << " ";
              }

              if(node.nodeWeight > 0 && (ew == 10|| ew == 11)) {
                  subGraph << node.nodeWeight << " ";
              }

              if(node.edgeWeight > 0 && (ew == 1|| ew == 11)) {
                  subGraph << node.edgeWeight << " ";
              }
          }
          subGraph << std::endl;
      }
      double timeLimit = (double)Buffer.size()/2000.0;
      //std::cout << "Time Limit: " << timeLimit << std::endl;
      //double timeLimit = 3600;
      //std::cout << "timeLimit: " << timeLimit << std::endl;

      if(decision == "KaMISBufferedRedumis") {
          //timeLimit = (double)Buffer.size()/292.4; // Average time per node of the non buffered KaMIS redumis with 13 different graphs
          //timeLimit = (double)Buffer.size()/225; // 30% more time for than just takin the average
          std::string command = ("./../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul");
          //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
          system(command.c_str());
      }
      if(decision == "KaMISBufferedOMis") {
          std::string command = ("./../KaMIS/deploy/online_mis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul");
          //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
          system(command.c_str());
      }
      if(decision == "KaMISBufferedWeightedBrRedu") {
          std::string command = ("./../KaMIS/deploy/weighted_branch_reduce ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul");
          //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
          system(command.c_str());
      }
      if(decision == "KaMISBufferedWeightedLS") {
          //timeLimit = (double)Buffer.size()/118.5; // Average time per node of the non buffered KaMIS local search with 13 different graphs
          //timeLimit = (double)Buffer.size()/91.154; // 30% more time than just taking the average
          std::string command = ("./../KaMIS/deploy/weighted_local_search ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul");
          //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
          system(command.c_str());
      }


      KaMISIndSet.open(IndSetName.c_str(), std::ios_base::app);

      if (!KaMISIndSet) {
              std::cerr << "Error opening (check IndSet)" << IndSetName << std::endl;
              exit(0);
      }

      std::string line;
      /*
      bool temp;
      unsigned index = 1;

      while(std::getline(KaMISIndSet, line)) {
          //std::cout << "index " << index << " index+offset " << index+offset << std::endl;
          //std::cout << "line: " << line << std::endl;
          temp = std::stoi(line); // if node at index+offset is in ind set or not
          IndSet[index+offset].in_IndSet = temp;
          marked[index+offset] = true; // mark node in any case, so it wont be in any future buffers
          if(temp == true) { // used to mark all neighbors of the node
              // used to find the correct node in the Buffer (and with that the neighbors)
              bool nodeFound = false;
              unsigned correctIndex;
              for(int i = 0; i < Memory.size(); i++) {
                  if(index+offset == Memory[i][0].ID) {
                      nodeFound = true;
                      correctIndex = i;
                      break;
                  }
              }
              if(!nodeFound) {
                  std::cerr << "Couldnt find node " << index+offset << " in the Buffer!"<< std::endl;
              } else {
                  for(int i = 1; i < Memory[correctIndex].size(); i++) {
                      marked[Memory[correctIndex][i].ID] = true;
                  }
              }
          }
          index++;
      }
      */

      // FROM OTHER FUNCTION
      bool node;
      unsigned index = 1;
      unsigned nmbNodesDecided = 0; // the number of nodes, for which we make a decission (>= buffersize)
      while(std::getline(KaMISIndSet, line)) {
          node = std::stoi(line);
          IndSet[inverse.get(index)].in_IndSet = node;
          if(!marked[inverse.get(index)]) {
              nmbNodesDecided++;
          }
          marked[inverse.get(index)] = true; // mark node in any case, so it wont be in any future buffers
          if(node == true) { // used to mark all neighbors of the node
              // used to find the correct node in the Buffer (and with that the neighbors)
              bool nodeFound = false;
              unsigned correctIndex;
              if(inverse.get(index) == Memory[index-1][0].ID) {
                  nodeFound = true;
                  correctIndex = index-1;
              }
              if(!nodeFound) {
                  std::cerr << "Couldnt find node " << inverse.get(index) << " in the Buffer!"<< std::endl;
              } else {
                  //std::cout << "index: " << index << " & correct index " << correctIndex<< std::endl;
                  for(int i = 1; i < Memory[correctIndex].size(); i++) {
                      if(!marked[Memory[correctIndex][i].ID]) {
                          nmbNodesDecided++;
                      }
                      marked[Memory[correctIndex][i].ID] = true;
                  }
              }
          }
          index++;
      }

      //std::ofstream outfile;
      //outfile.open("nmbNodesDecided.txt", std::ios_base::app);
      //outfile << Buffer.size() << "/" << nmbNodesDecided << "/" << ((float)(nmbNodesDecided)*100.0)/(float)(Buffer.size()) << "%" << std::endl;
      inverse.clear();
  }

// generates a kamis indset of a subgraph withour using the harddrive, see funcions in KaMIS_API.cpp
 void generateSubgraph(std::vector<std::vector<BufferNode>>& Buffer, unsigned Buffersize, std::string decision, int ew, std::vector<IndNode>& IndSet, std::vector<bool>& marked, InverseMapping& inverse, InverseMapping& normalize) {
     //std::cout << "Copy the Buffer" << std::endl;
     std::vector<std::vector<BufferNode>> Memory = Buffer; // to know later which neighbors to mark (center node in IndSet)

     //std::cout << "Create inverse Mapping, remove future nodes and normalize indices" << std::endl;
     createValidGraph(Buffer, inverse, normalize);
     //getMapping(Buffer, inverse);
     //std::cout << "starting to get a offset " << std::endl;
     //int offset = getOffset(Buffer);

     // Enables check for Subgraphs
     // Works currently only with -j1 as option for parallel (only one core used)

     //checkBufferedGraph(Buffer, "graphChecker.txt", ew);

     //removeFutureNodes(Buffer);
     //removeFutureNodes(Buffer, inverse);
     /*
     std::cout << Buffer.size() << " " << countBufferEdges(Buffer) << " " << ew << std::endl;
     for(std::vector<BufferNode> neighbors : Buffer) {
         //std::cout << "- ";
         for(int i = 0; i < neighbors.size(); i++) {
             BufferNode node = neighbors[i];
             if(i > 0 || true) {
                 std::cout << node.ID << " ";
             }

             if(node.nodeWeight > 0 && (ew == 10|| ew == 11)) {
                 std::cout << node.nodeWeight << " ";
             }

             if(node.edgeWeight > 0 && (ew == 1|| ew == 11)) {
                 std::cout << node.edgeWeight << " ";
             }
         }
         std::cout << std::endl;
     }
     std::cout << std::endl << std::endl;
     */


     std::ofstream subGraph;
     std::ifstream KaMISIndSet;


     /*
     //remove(subGraphName.c_str());
     //subGraph.close();
     // empty the file from last run
     unsigned edges = countBufferEdges(Buffer);

     std::cout << Buffer.size() << " " << edges << " " << ew << std::endl;
     for(std::vector<BufferNode> neighbors : Buffer) {
         std::cout << "- ";
         for(int i = 0; i < neighbors.size(); i++) {
             BufferNode node = neighbors[i];
             if(i > 0) {
                 std::cout << node.ID << " ";
             }

             if(node.nodeWeight > 0 && (ew == 10|| ew == 11)) {
                 std::cout << node.nodeWeight << " ";
             }

             if(node.edgeWeight > 0 && (ew == 1|| ew == 11)) {
                 std::cout << node.edgeWeight << " ";
             }
         }
         std::cout << std::endl;
     }
     std::cout << std::endl << std::endl;
     */
     //std::cout << "generate Subgraph G" << std::endl;
     graph_access G = genereteSubgraphAccess(Buffer);
     //std::cout << "generate Subgraph fG" << std::endl;
     graph_access fG = genereteSubgraphAccess(Buffer);

     // USED FOR DEGUBBIGN
     /*
     forall_nodes(G, node) {
         std::cout << node << "(" << G.getNodeDegree(node) << ") ";
         forall_out_edges(G, edge, node) {
             std::cout << G.getEdgeTarget(edge) << " ";
         }endfor
         std::cout << std::endl;
     }endfor
     */
     //std::cout << "Generate IndSet" << std::endl;
     //double timeLimit = (double)Buffer.size()/1000.0;
     //double timeLimit = 3600;
     //std::cout << "timeLimit: " << timeLimit << std::endl;

     // "Disable" Console output
     std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
     //std::ofstream   fout("/dev/null");
     std::ofstream   fout("nul");
     std::cout.rdbuf(fout.rdbuf()); // redirect 'cout' to a 'fout'
     // ...


     std::vector<bool> SubgraphIndSet;
     if(decision == "KaMISBufferedRedumis") {
         //timeLimit = (double)Buffer.size()/292.4; // Average time per node of the non buffered KaMIS redumis with 13 different graphs
         //timeLimit = (double)Buffer.size()/225; // 30% more time for than just takin the average
         //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --change_PartMode="+std::to_string(PartitionMode)+" --output="+IndSetName+" > nul:");
         //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
         //system(command.c_str());
         SubgraphIndSet = createIndSet_REDUMIS(G);

     }
     if(decision == "KaMISBufferedOMis") {
         //std::string command = ("./../../KaMIS/deploy/online_mis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul:");
         //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
         //system(command.c_str());
         SubgraphIndSet = createIndSet_OMIS(G, fG);
     }
     if(decision == "KaMISBufferedWeightedBrRedu") {
         //std::string command = ("./../../KaMIS/deploy/weighted_branch_reduce ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul:");
         //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
         //system(command.c_str());
     }
     if(decision == "KaMISBufferedWeightedLS") {
         //timeLimit = (double)Buffer.size()/118.5; // Average time per node of the non buffered KaMIS local search with 13 different graphs
         //timeLimit = (double)Buffer.size()/91.154; // 30% more time than just taking the average
         //std::string command = ("./../../KaMIS/deploy/weighted_local_search ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul:");
         //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
         //system(command.c_str());
     }
     if(decision == "BufferedLubyDegree") {
         SubgraphIndSet = BufferedLubyDegree(G);
         //timeLimit = (double)Buffer.size()/118.5; // Average time per node of the non buffered KaMIS local search with 13 different graphs
         //timeLimit = (double)Buffer.size()/91.154; // 30% more time than just taking the average
         //std::string command = ("./../../KaMIS/deploy/weighted_local_search ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --output="+IndSetName+" > nul:");
         //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
         //system(command.c_str());
     }

     // "Enable" Console output
     std::cout.rdbuf(cout_sbuf); // restore the original stream buffer
     //std::cout << "Ind Set found!" << std::endl;
     //std::cout << "Mark Neighbors" << std::endl;
     unsigned index = 1;
     unsigned nmbNodesDecided = 0; // the number of nodes, for which we make a decission (>= buffersize)
     for(bool node : SubgraphIndSet) {
         IndSet[inverse.get(index)].in_IndSet = node;
         if(!marked[inverse.get(index)]) {
             nmbNodesDecided++;
         }
         marked[inverse.get(index)] = true; // mark node in any case, so it wont be in any future buffers
         if(node == true) { // used to mark all neighbors of the node
             // used to find the correct node in the Buffer (and with that the neighbors)
             bool nodeFound = false;
             unsigned correctIndex;
             if(inverse.get(index) == Memory[index-1][0].ID) {
                 nodeFound = true;
                 correctIndex = index-1;
             }
             if(!nodeFound) {
                 std::cerr << "Couldnt find node " << inverse.get(index) << " in the Buffer!"<< std::endl;
             } else {
                 //std::cout << "index: " << index << " & correct index " << correctIndex<< std::endl;
                 for(int i = 1; i < Memory[correctIndex].size(); i++) {
                     if(!marked[Memory[correctIndex][i].ID]) {
                         nmbNodesDecided++;
                     }
                     marked[Memory[correctIndex][i].ID] = true;
                 }
             }
         }
         index++;
     }

     //std::ofstream outfile;
     //outfile.open("nmbNodesDecided.txt", std::ios_base::app);
     //outfile << Buffer.size() << "/" << nmbNodesDecided << "/" << ((float)(nmbNodesDecided)*100.0)/(float)(Buffer.size()) << "%" << std::endl;
     inverse.clear();
     //normalize.clear();
     //std::cout << "Done!" << std::endl;
 }

// generates a graph_access object, used by generate subgraph ( to not rely on harddrive)
 graph_access genereteSubgraphAccess(std::vector<std::vector<BufferNode>>& Buffer) {
     graph_access G;
     unsigned edges = 2*countBufferEdges(Buffer);
     unsigned nodes = Buffer.size();
     G.start_construction(nodes, edges);
     NodeID node_G;
     for(int i = 0; i < Buffer.size(); i++) {
         node_G = G.new_node();
         //std::cout << "new node(degree) " <<  node_G << "(" << G.getNodeDegree(node_G) << ") " << std::endl;
         G.setPartitionIndex(node_G, 0);
         G.setNodeWeight(node_G, Buffer[i][0].nodeWeight); // set node weight of current node
         for(int j = 1; j < Buffer[i].size(); j++) {
             EdgeID e = G.new_edge(node_G, Buffer[i][j].ID-1);
             //std::cout << "Edge from " << node_G << " to " << Buffer[i][j].ID-1 << std::endl;
             //std::cout << node_G << "(" << G.getNodeDegree(node_G) << ") " << std::endl;
             G.setEdgeWeight(e, Buffer[i][j].edgeWeight);
         }
         //std::cout <<"  "<< node_G << "(" << G.getNodeDegree(node_G) << ") " << std::endl;
     }
     G.finish_construction();
     return G;
 }


// normalizes indices, removes future nodes (needs "normalize map") and creates inverse mapping (needed later)
void createValidGraph(std::vector<std::vector<BufferNode>>& Buffer, InverseMapping& inverse) {
    std::map<unsigned, unsigned> normalize; // takes original node IDs and translates them to the normalized ones
    //create normalize map and inverse mapping
    for(unsigned i = 0; i < Buffer.size(); i++) {
        normalize[Buffer[i][0].ID] = i+1;
        inverse.set(i+1, Buffer[i][0].ID);
        //std::cout << "normalize: " << Buffer[i][0].ID << " maps to " << i+1 << "(== "<< normalize[Buffer[i][0].ID] << " )"<< std::endl;
        //std::cout << "invere:    " << i+1 << " maps to " << Buffer[i][0].ID << " (== " << inverse.get(i+1) << " )" << std::endl;
        //std::cout << i+1 << " -> " << original[i+1]  << "(" << Buffer[i][0].ID << ")"<< std::endl;
    }
    // remove future nodes with "normalize map"
    for(std::vector<BufferNode>& neighbors : Buffer) {
        // for(BufferNode& node : neighbors) {
        for(int i = 1; i < neighbors.size(); i++) {
            //node.ID = normalize[node.ID];
            if(normalize.count(neighbors[i].ID) != 1) { // if Future node
            //if(!normalize.contains(neighbors[i].ID)) { // if Future node
                // push each of the following neighbors of this "center node" one position to the front
                for(int j = i; j < neighbors.size()-1; j++) {
                    neighbors[j] = neighbors[j+1];
                }
                i = i-1; // to check the current position again (since following neighbor got moved here)
                neighbors.pop_back();
            }
        }
    }
    // Check InverseMapping with normalize map
    for(unsigned i = 0; i < Buffer.size(); i++) {
        if(inverse.get(normalize[Buffer[i][0].ID]) != Buffer[i][0].ID) {
            std::cout << "Normalize and Inverse Mapping are contradicting" << std::endl;
            std::cout << "    i: " << i << ", normalize: "<< normalize[Buffer[i][0].ID] << ", " << inverse.get(normalize[Buffer[i][0].ID]) << " != " << Buffer[i][0].ID << std::endl;
        }
    }

    // normalize the remaining graph
    for(std::vector<BufferNode>& neighbors : Buffer) {
        for(BufferNode& node : neighbors) {
            node.ID = normalize[node.ID];
        }
    }



}


// normalizes indices, removes future nodes (needs "normalize map") and creates inverse mapping (needed later)
void createValidGraph(std::vector<std::vector<BufferNode>>& Buffer, InverseMapping& inverse, InverseMapping& normalize) {
    //std::map<unsigned, unsigned> normalize; // takes original node IDs and translates them to the normalized ones
    //create normalize map and inverse mapping
    for(unsigned i = 0; i < Buffer.size(); i++) {
        normalize.set(Buffer[i][0].ID, i+1);
        inverse.set(i+1, Buffer[i][0].ID);
        //std::cout << "normalize: " << Buffer[i][0].ID << " maps to " << i+1 << "(== "<< normalize[Buffer[i][0].ID] << " )"<< std::endl;
        //std::cout << "invere:    " << i+1 << " maps to " << Buffer[i][0].ID << " (== " << inverse.get(i+1) << " )" << std::endl;
        //std::cout << i+1 << " -> " << original[i+1]  << "(" << Buffer[i][0].ID << ")"<< std::endl;
    }
    // remove future nodes with "normalize map"
    for(std::vector<BufferNode>& neighbors : Buffer) {
        // for(BufferNode& node : neighbors) {
        for(int i = 1; i < neighbors.size(); i++) {
            //node.ID = normalize[node.ID];
            if(normalize.get(neighbors[i].ID) == -1) { // if Future node
            //if(!normalize.contains(neighbors[i].ID)) { // if Future node
                // push each of the following neighbors of this "center node" one position to the front
                for(int j = i; j < neighbors.size()-1; j++) {
                    neighbors[j] = neighbors[j+1];
                }
                i = i-1; // to check the current position again (since following neighbor got moved here)
                neighbors.pop_back();
            }
        }
    }
    // Check InverseMapping with normalize map
    for(unsigned i = 0; i < Buffer.size(); i++) {
        if(inverse.get(normalize.get(Buffer[i][0].ID)) != Buffer[i][0].ID) {
            std::cout << "Normalize and Inverse Mapping are contradicting" << std::endl;
            std::cout << "    i: " << i << ", normalize: "<< normalize.get(Buffer[i][0].ID) << ", " << inverse.get(normalize.get(Buffer[i][0].ID)) << " != " << Buffer[i][0].ID << std::endl;
        }
    }

    // normalize the remaining graph
    for(std::vector<BufferNode>& neighbors : Buffer) {
        for(BufferNode& node : neighbors) {
            node.ID = normalize.get(node.ID);
        }
    }
    normalize.clear();

}

// counts the remaining edges of the Buffered Subgraph after normalizing
unsigned countBufferEdges(std::vector<std::vector<BufferNode>>& Buffer) {
    unsigned edges = 0;
    for(std::vector<BufferNode>& neighbors : Buffer) {
        for(BufferNode& node : neighbors) {
            if(node.ID > neighbors[0].ID) {
                edges++;
            }
        }
    }
    return edges;
}

// returns the individual name of the buffered Subgraph (used by bufferedKaMIS)
std::string bufferedSubGraphName(std::string filename, std::string decision, unsigned Buffersize) {
    std::vector<std::string> path = splitPath(filename);
    //std::string outputfilename = path[1]+"#"+decision+"#"+std::to_string(Buffer.size())+"#"+std::to_string(rndm()%4096)+".txt";
    return path[1]+"#"+decision+"#"+std::to_string(Buffersize)+".txt";
}
// returns the individual name of the buffered Independent set returned by KaMIS (used by bufferedKaMIS)
std::string bufferedIndSetName(std::string filename, std::string decision, unsigned Buffersize) {
    std::vector<std::string> path = splitPath(filename);
    //std::string outputfilename = path[1]+"#"+decision+"#"+std::to_string(Buffer.size())+"#"+std::to_string(rndm()%4096)+".txt";
    return "IndSet"+path[1]+"#"+decision+"#"+std::to_string(Buffersize)+".txt";
}

// just runs trivial degree_luby on a buffered subgraph (not choosing greedy the smallest_degre nodes from a buffered subgraph!)
std::vector<bool> BufferedLubyDegree(graph_access& G) {
    std::vector<bool> marked;
    std::vector<bool> IndSet;
    marked.resize(G.number_of_nodes());
    IndSet.resize(G.number_of_nodes());

    while(!isMaxml(IndSet, G)) {
        forall_nodes(G, node) {
            bool move_node = true;
            //std::cout << node << "(" << G.getNodeDegree(node) << ") ";
            if(!marked[node]) {
                forall_out_edges(G, edge, node) {
                    //std::cout << G.getEdgeTarget(edge) << " ";
                    if(!marked[G.getEdgeTarget(edge)]) {
                        if(G.getNodeDegree(G.getEdgeTarget(edge)) < G.getNodeDegree(node)) {
                            move_node = false;
                            break;
                        }
                    }
                }endfor
                // add current node to IndSet and mark the node and all its neigbors
                if(move_node) {
                    IndSet[node] = true;
                    marked[node] = true;
                    forall_out_edges(G, edge, node) {
                        marked[G.getEdgeTarget(edge)] = true;
                    }endfor
                }
            }
            //std::cout << std::endl;
        }endfor
    }
    return IndSet;
}

void checkBufferedGraph(std::vector<std::vector<BufferNode>>& Buffer, std::string subGraphName, int weights) {
    std::ofstream subGraph;
    subGraph.open(subGraphName, std::ios::out | std::ios::trunc);
    subGraph.close();
    subGraph.open(subGraphName, std::ios_base::app);
    unsigned edges = countBufferEdges(Buffer);
    subGraph << Buffer.size() << " " << edges << " " << weights << std::endl;
    for(std::vector<BufferNode> neighbors : Buffer) {
        for(int i = 0; i < neighbors.size(); i++) {
            BufferNode node = neighbors[i];
            if(i > 0) {
                subGraph << node.ID << " ";
            }

            if(node.nodeWeight > 0 && (weights == 10|| weights == 11)) {
                subGraph << node.nodeWeight << " ";
            }

            if(node.edgeWeight > 0 && (weights == 1|| weights == 11)) {
                subGraph << node.edgeWeight << " ";
            }
        }
        subGraph << std::endl;
    }
    //std::string command = ("./../../KaMIS/deploy/graphchecker ./"+subGraphName);
    //std::string command = ("./../../KaMIS/deploy/redumis ./"+subGraphName+" --time_limit="+std::to_string(timeLimit)+" --console_log");
    //system(command.c_str());

    // custom Graphchecker

    std::string line;


    // open file for reading
    std::ifstream in(subGraphName.c_str());
    if (!in) {
        std::cerr << "Error opening " << subGraphName << std::endl;
        return;
    }
    /*
    std::cout << "=========================================="                           << std::endl;
    std::cout << "\t\tGraphChecker"                                                     << std::endl;
    std::cout << "=========================================="                           << std::endl;
    std::cout <<  "Note: Output will be given using the IDs from file, i.e. the IDs are starting from 1."  << std::endl;
    std::cout <<  std::endl;
    */
    std::getline(in,line);
    //skip comments
    while (line[0] == '%') {
        std::getline(in, line);
    }

    long nmbNodes;
    long nmbEdges;
    long ew = 0;

    std::stringstream ss(line);
    ss >> nmbNodes;
    ss >> nmbEdges;
    ss >> ew;

    std::vector<long> node_starts;
    node_starts.reserve(nmbNodes + 1);
    node_starts.push_back(0);

    std::vector<long> adjacent_nodes;
    adjacent_nodes.reserve(nmbEdges * 2);

    long node_weight;
    long total_nodeweight = 0;

    long node_degree = 0;
    long node_counter = 0;
    long edge_counter = 0;

    bool node_weights = false;
    bool edge_weights = false;
    if (ew == 11) {
        node_weights = true;
        edge_weights = true;
    } else if (ew == 10) {
        node_weights = true;
    } else if (ew == 1) {
        edge_weights = true;
    }

    while (std::getline(in, line)) {
        // Check if there are more nodes than specified
        if (node_counter > nmbNodes) {
            std::cout <<  "There are more nodes in the file than specified in the first line of the file."  << std::endl;
            std::cout <<  "You specified " <<  nmbNodes << " nodes." << std::endl;
            std::cout <<  node_counter  << std::endl;
            exit(0);
        }

        if (line[0] == '%') continue;

        node_degree = 0;

        std::stringstream ss(line);

        if (node_weights) {
            ss >> node_weight;
            if (node_weight < 0) {
                std::cout <<  "The node " <<  node_counter+1 << " has weight < 0."  << std::endl;
                std::cout <<  "See line " << node_counter+2 << " of your file."  << std::endl;
                std::cout <<  "*******************************************************************************"  << std::endl;
                exit(0);
            }
            total_nodeweight += node_weight;
            if (total_nodeweight > (long)std::numeric_limits<unsigned int>::max()) {
                std::cout <<  "The sum of the node weights exeeds 32 bits. Currently not supported."  << std::endl;
                std::cout <<  "Please scale weights of the graph."  << std::endl;
                std::cout <<  "*******************************************************************************"  << std::endl;
                exit(0);
            }
        }


        long target;
        while (ss >> target) {
            node_degree++;
            // Check if the neighboring node is within the valid range
            if (target > nmbNodes || target <= 0) {
                std::cout <<  "Node " << node_counter + 1
                          << " has an edge to a node greater than the number of nodes specified in the file or smaller or equal to zero, i.e. it has target "
                          <<  target << " and the number of nodes specified was " <<  nmbNodes << std::endl;                        std::cout <<  "See line " << node_counter + 2 << " of your file."  << std::endl;
                exit(0);
            }

            adjacent_nodes.push_back(target - 1);


            if (edge_weights) {
                long edge_weight = 1;
                if (ss.eof()) {
                    std::cout <<  "Something is wrong."  << std::endl;
                    std::cout <<  "See line " << node_counter+2 << " of your file."  << std::endl;
                    std::cout <<  "There is not the right amount of numbers in line " << node_counter+2 << " of the file. " << std::endl;
                    if (node_weights) {
                        std::cout <<  "That means either the node weight is missing, "
                                  <<  "or there is an edge without a weight specified, "
                                  <<  "or there are no edge weights at all despite the specification "
                                  <<  ew  << " in the first line of the file."<< std::endl;
                        std::cout <<  "*******************************************************************************"  << std::endl;
                    } else {
                        std::cout <<  "That there is may be an edge without an edge weight specified "
                                  <<  "or there are no edge weights at all despite the specification "
                                  <<  ew  << " in the first line of the file."<< std::endl;
                        std::cout <<  "*******************************************************************************"  << std::endl;

                    }
                    exit(0);

                }
                ss >> edge_weight;
            }
            edge_counter++;
        }
        node_counter++;
        node_starts.push_back(node_starts.back() + node_degree);

        if (in.eof()) break;
    }
    //std::cout <<  "IO done. Now checking the graph .... "  << std::endl;


    // Check if right number of nodes was detected
    if (node_counter != nmbNodes) {
        std::cout <<  "The number of nodes specified in the beginning of the file "
                  <<  "does not match the number of nodes that are in the file."  << std::endl;
        std::cout <<  "You specified " <<  nmbNodes <<  " but there are " <<  node_counter  << std::endl;
        exit(0);
    }

    // Check if right number of edges was detected
    if (edge_counter != 2 * nmbEdges || node_starts.back() != 2 * nmbEdges) {
        std::cout <<  "The number of edges specified in the beginning of the file "
                  <<  "does not match the number of edges that are in the file."  << std::endl;
        std::cout <<  "You specified " <<  2 * nmbEdges <<  " but there are " <<  edge_counter << std::endl;
        exit(0);
    }


    // Check if there are parallel edges
    for (long node = 0; node < nmbNodes; node++) {
        std::unordered_set<long> seen_adjacent_nodes;
        for (long e = node_starts[node]; e < node_starts[node + 1]; e++) {
            long target = adjacent_nodes[e];
            if (!seen_adjacent_nodes.insert(target).second) {
                std::cout <<  "The file contains parallel edges."  << std::endl;
                std::cout <<  "In line " <<  node + 2 << " of the file " <<  target + 1 << " is listed twice."   << std::endl;
                exit(0);
            }

            // Check for sorted order
            if (e > node_starts[node]) {
                long prev_target = adjacent_nodes[e - 1];
                if (prev_target > target) {
                    std::cout <<  "The file contains unsorted edges."  << std::endl;
                    std::cout <<  "In line " <<  node + 2 << " of the file " <<  target + 1
                              <<  " is greater than " << prev_target + 1 << "."  << std::endl;
                    exit(0);
                }
            }

            // Check for self loops
            if (target == node) {
                std::cout <<  "The file contains a graph with self-loops."  << std::endl;
                std::cout <<  "In line " <<  node + 2 << " of the file (node="
                          << node + 1 << ") the target " << target + 1 << " is listed."   << std::endl;
                exit(0);
            }
        }
    }

    // Check if all backward and forward edges exist
    for (long node = 0; node < nmbNodes; node++) {
        for (long e = node_starts[node]; e < node_starts[node + 1]; e++) {
            long target = adjacent_nodes[e];
            bool found = false;
            for (long e_bar = node_starts[target]; e_bar < node_starts[target + 1]; e_bar++) {
                if (adjacent_nodes[e_bar] == node) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                std::cout <<  "The file does not contain all forward and backward edges. "  << std::endl;
                std::cout <<  "Node " <<  node + 1 << " (line " << node + 2
                          <<  ") does contain an edge to node " << target + 1 << " but there is no edge ("
                          <<  target + 1 << "," << node + 1 << ") in the file. "<<  std::endl;
                std::cout <<  "Please insert this edge in line " << target + 2 << " of the file." << std::endl;
                exit(0);
            }
        }
    }
    /*
    std::cout << std::endl;
    std::cout << "=========================================="                           << std::endl;
    std::cout <<  "The graph format seems correct."  << std::endl;
    */
}
