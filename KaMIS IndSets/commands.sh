path_to_kamis="./../../../KaMIS/deploy/"
echo "" > KamisSets.txt
for path in `cat paths.txt`; do
for algo in online_mis; do
	file="${path##*/}"
	temp="${path%/*}"
	dir="${temp##*/}"
	echo "$path_to_kamis$algo $path --time_limit=3600 --output=$algo#$file#$dir.txt > nul:"
	echo "$algo$path" >> KamisSets.txt
done
done > commands.txt

#redumis online_mis weighted_branch_reduce weighted_local_search
