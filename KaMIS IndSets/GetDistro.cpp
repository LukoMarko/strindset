
#include "graph_io.h"
#include <sstream>


int main(int argn, char **argv) {
    if(argn != 2) {
        std::cout <<  "Usage: distro FILENAME"  << std::endl;
        exit(0);
    }
    std::string kamisDistro = argv[1];
    std::string writeTo = "Distrib.txt";
    std::ifstream in(kamisDistro.c_str());
    std::string line;
    if (!in) {
            std::cerr << "Error opening (kamisDistro)" << "Distrib.txt" << std::endl;
            exit(0);
    }
    std::getline(in,line);
    std::string index;
    std::string value;
    //std::cout << "index: " << index << std::endl << "value: " << value << std::endl;
    //std::cout << line << std::endl;
    std::vector<double> Distribution;
    std::vector<double> IndividualSampleCount;
    Distribution.resize(30);
    IndividualSampleCount.resize(30);
    double sampleCount = 0;
    while(std::getline(in, line)) {
        std::stringstream ss(line);
        ss >> index;
        ss >> value;

        if(value != "0" && value != "-nan") {
            Distribution[atoi(index.c_str())] += std::strtod(value.c_str(), nullptr);
            IndividualSampleCount[atoi(index.c_str())]++;
        }
    }
    for(int i = 0; i < Distribution.size(); i++) {
        Distribution[i] /= IndividualSampleCount[i];
    }
    for(int i = 0; i < Distribution.size(); i++) {
        std::cout << i << " " << Distribution[i]<< std::endl;
    }




}
/*

void getGraph(graph_access& G, const std::string& filename) {
    std::string line;

    // open file for reading
    std::ifstream in(filename.c_str());
    if (!in) {
            std::cerr << "Error opening (get Graph)" << filename << std::endl;
            exit(0);
    }

    long nmbNodes;
    long nmbEdges;

    std::getline(in,line);
    //skip comments
    while( line[0] == '%' ) {
            std::getline(in, line);
    }

    int ew = 0;
    std::stringstream ss(line);
    // read in first three numbers
    // number of nodes, number of edges, graph conf. parameter
    ss >> nmbNodes;
    ss >> nmbEdges;
    ss >> ew;

    bool read_ew = false;
    bool read_nw = false;
    // check graph conf. parameter
    if(ew == 1) {
            read_ew = true; //graph has (only) edge weights
    } else if (ew == 11) {
            read_ew = true; // graph has edge weights
            read_nw = true; // and node weights
    } else if (ew == 10) {
            read_nw = true; // graph has (only) node weights
    }
    nmbEdges *= 2; //since we have forward and backward edges

    NodeID node_G;
    G.start_construction(nmbNodes, nmbEdges);
    while(std::getline(in, line)) {
            if (line[0] == '%') { // a comment in the file
                    continue;
            }

            std::stringstream ss(line);

            NodeWeight weight = 1;
            if( read_nw ) {
                    ss >> weight;
            }
            node_G = G.new_node();
            G.setPartitionIndex(node_G, 0);
            G.setNodeWeight(node_G, weight);

            NodeID target;
            while( ss >> target ) {

                    EdgeWeight edge_weight = 1;
                    if( read_ew ) {
                            ss >> edge_weight;
                    }
                    EdgeID e = G.new_edge(node_G, target-1);
                    G.setEdgeWeight(e, edge_weight);
            }
            if(in.eof()) {
                    break;
            }
    }
    G.finish_construction();
}

//checks the indSet returned from Kamis
void check_IndSet(graph_access& G, std::string writeTo, std::string kamisSet, std::string algorithm, std::string graphname) {
    // first check the ind set
    NodeID index = 0;
    bool is_maximal = true;
    bool is_independent = true;

    NodeWeight weight_sum = 0;
    NodeWeight total_weight_sum = 0;
    NodeID number_of_nodes = 0;
    NodeID IndSet_neighbor;

    std::ifstream in(kamisSet.c_str());
    std::string line;
    if (!in) {
            std::cerr << "Error opening (check IndSet)" << kamisSet << std::endl;
            exit(0);
    }
    std::vector<bool> IndSet;
    IndSet.reserve(G.number_of_nodes());
    bool temp;
    while(std::getline(in, line)) {
        temp = std::stoi(line);
        IndSet.push_back(temp);
    }

    //Additional output starts here
    std::ofstream output;
    //std::remove((writeTo+".txt").c_str());
    output.open(writeTo+".txt", std::ios_base::app);
    //output << graphname << std::endl << algorithm << std::endl << std::endl;

    std::vector<unsigned> IndSetNodesDegree;
    std::vector<unsigned> VertexCoverNodesDegree;
    double VertexCoverAverageDegree;
    double IndSetAverageDegree;
    // for loop to find to find max degree and average degree
    unsigned IndSetMaxDegree = 0;
    unsigned VertexCoverMaxDegree = 0;
    double IndSetNmbrNodes = 0;
    double VertexCoverNmbrNodes = 0;
    for(bool node : IndSet) {
        if(node) {
            IndSetNmbrNodes++;
            unsigned currentDegree = G.getNodeDegree(index);
            IndSetAverageDegree += currentDegree;
            if(IndSetMaxDegree < currentDegree){
                IndSetMaxDegree = currentDegree;
            }
        } else {
            VertexCoverNmbrNodes++;
            unsigned currentDegree = G.getNodeDegree(index);
            VertexCoverAverageDegree += currentDegree;
            if(VertexCoverMaxDegree < currentDegree) {
                VertexCoverMaxDegree = currentDegree;
            }
        }
        index++;
    }
    float fl_IndSetAverageDegree = (float)((float)IndSetAverageDegree/(float)IndSetNmbrNodes);
    float fl_VertexCoverAverageDegree = (float)((float)VertexCoverAverageDegree/(float)VertexCoverNmbrNodes);
    //IndSetAverageDegree = IndSetAverageDegree/IndSetNmbrNodes;
    //VertexCoverAverageDegree = VertexCoverAverageDegree/VertexCoverNmbrNodes;
    float  fl_AverageNodeDegree = (float) (((float)(IndSetAverageDegree+VertexCoverAverageDegree))/((float)(IndSetNmbrNodes+VertexCoverNmbrNodes)));
    VertexCoverNodesDegree.resize(VertexCoverMaxDegree+1);
    IndSetNodesDegree.resize(IndSetMaxDegree+1);

    index = 0;
    for(bool node : IndSet) {
        if(node) {
            IndSetNodesDegree[G.getNodeDegree(index)] += 1;
        } else {
            VertexCoverNodesDegree[G.getNodeDegree(index)] += 1;
        }
        index++;
    }
    //output << "Average Node Degree: " <<fl_AverageNodeDegree << std::endl;
    //output << "Average Ind Set Degree: " << fl_IndSetAverageDegree << std::endl;
    //output << "Average Vertex Cover Degree: " << fl_VertexCoverAverageDegree << std::endl;
    //output << std::endl;
    //plotData(output, IndSetNodesDegree,VertexCoverNodesDegree, "Ind Set Node Degree", "Vertex Cover Node Degree", 3);
    plotDistribution(output, IndSetNodesDegree,VertexCoverNodesDegree);
    //plotData(output, VertexCoverNodesDegree, "Vertex Cover Node Degree", 3);
    /*
    for(bool node : IndSet) {
        // check if Ind Set is maximal
        // (every node that is not in the ind set has at least one
        // neighbor in the ind set)
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // check if Ind Set is independent
        // (Every node in the Ind Set has no neighbor that is also
        // in the Ind Set)
        bool has_IndSet_neighbor = false;
        forall_out_edges(G,e,index) {
            if(G.getEdgeTarget(e) != index) {
                if(IndSet[G.getEdgeTarget(e)]) {
                    has_IndSet_neighbor = true;
                    IndSet_neighbor = G.getEdgeTarget(e);
                    break;
                }
            }
        }endfor
        if(!has_IndSet_neighbor && !node) {
            is_maximal = false;
        }
        if(has_IndSet_neighbor && node) {
            is_independent = false;
        }

        // create information of the Ind set
        total_weight_sum += G.getNodeWeight(index);
        if(node) {
            number_of_nodes++;
            weight_sum += G.getNodeWeight(index);
        }
        index++;
    }
    */
    /*
    output << "Max:" << is_maximal << " Ind:" << is_independent;
    output << " Nds:" << number_of_nodes << "/" << index << " (" << ((float)(number_of_nodes)*100.0)/(float)(index) << "%)";
    output << " Wght:" << weight_sum << "/" << total_weight_sum << " (" << ((float)(weight_sum)*100.0)/(float)(total_weight_sum) << "%)";

    output << std::endl;
}

void plotData(std::ofstream& output, std::vector<unsigned>& data, std::string title, unsigned offset) {
    output << title << std::endl << std::endl;
    unsigned maxSymbols = 180;
    unsigned maxSample = data[0];
    for(unsigned sample : data) {
        if(sample > maxSample) {
            maxSample = sample;
        }
    }
    double factor = (float)(maxSymbols)/(float)(maxSample);
    output << "x := " << (float)(maxSample)/(float)(maxSymbols) << std::endl;
    //std::cout << factor << std::endl;
    unsigned index = 0;
    for(unsigned sample : data) {
        if(sample*factor > offset) {
            output << index << " ";
            //std::cout << sample*factor << std::endl;
            for(int i = 0; i < sample*factor; i++) {
                output << "x";
            }
            output << std::endl;
        }
        index++;
    }
    output << std::endl;

}

void plotData(std::ofstream& output, std::vector<unsigned>& data1, std::vector<unsigned>& data2, std::string title1, std::string title2, unsigned offset) {
    unsigned maxSymbols = 180;
    unsigned maxSample = data1[0];
    for(unsigned sample : data1) {
        if(sample > maxSample) {
            maxSample = sample;
        }
    }
    for(unsigned sample : data2) {
        if(sample > maxSample) {
            maxSample = sample;
        }
    }
    double factor = (float)(maxSymbols)/(float)(maxSample);
    output << "x := " << (float)(maxSample)/(float)(maxSymbols) << std::endl;
    //std::cout << factor << std::endl;
    output << title1 << std::endl << std::endl;
    unsigned index = 0;
    for(unsigned sample : data1) {
        if(sample*factor > offset) {
            output << index << " ";
            //std::cout << sample*factor << std::endl;
            for(int i = 0; i < sample*factor; i++) {
                output << "x";
            }
            output << std::endl;
        }
        index++;
    }
    output << std::endl << std::endl;
    output << title2 << std::endl << std::endl;
    index = 0;
    for(unsigned sample : data2) {
        if(sample*factor > offset) {
            output << index << " ";
            //std::cout << sample*factor << std::endl;
            for(int i = 0; i < sample*factor; i++) {
                output << "x";
            }
            output << std::endl;
        }
        index++;
    }
    output << std::endl;
}

void plotDistribution(std::ofstream& output, std::vector<unsigned>& data1, std::vector<unsigned>& data2) {
    double ratio;
    for(int i = 0; i <= 30; i++) {
        output << i << " ";
        ratio = (float)(data1[i])/(float)(data1[i]+data2[i]);
        output << ratio;
    }
}
*/
