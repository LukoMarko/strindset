
#include "graph_io.h"
#include <sstream>

//ls | grep '#' | parallel ./port


std::string getFile(std::string filename);
std::string getAlgorithm(std::string filename);
std::string getDir(std::string filename);
void getGraph(graph_access& G, const std::string& filename);
void check_IndSet(graph_access& G, std::string writeTo, std::string kamisSet, std::string algorithm);

int main(int argn, char **argv) {
    if(argn != 2) {
        std::cout <<  "Usage: port FILENAME"  << std::endl;
        exit(0);
    }
    std::string kamisSet = argv[1];
    std::string writeTo = getFile(argv[1]);
    std::string algorithm = getAlgorithm(argv[1]);
    std::string readFrom = "./../../examples/"+getDir(argv[1])+"/"+getFile(argv[1]);
    std::cout << "KamisSet: " << kamisSet << std::endl;
    std::cout << "File " << writeTo << std::endl;
    std::cout << "Filename of graph: " << readFrom << std::endl;
    graph_access G;
    getGraph(G, readFrom);
    check_IndSet(G, writeTo, kamisSet, algorithm);
    //std::remove(file+".txt");
}

// Takes filename created from the KaMIS Program and returns the graph name
// to later store the information of the InsSet
std::string getFile(std::string filename) {
    bool reachedHashTag = false;
    std::string file;
    for(int i = 0; i < filename.size(); i++) {
        if(filename[i] == '#' && !reachedHashTag) {
            reachedHashTag = true;
            continue;
        }
        if(reachedHashTag) {
            if(filename[i] == '#') {
                break;
            }
            file.push_back(filename[i]);
        }
    }
    return file;
}

// Takes the filename created from the KaMIS Program and returns the algorithm
// embedded inside the filename
std::string getAlgorithm(std::string filename) {
    std::string algo;
    for(int i = 0; i < filename.size(); i++) {
        if(filename[i] == '#') {
            break;
        }
        algo.push_back(filename[i]);
    }
    return algo;
}

// Takes the kamis filename and returns the directory in which the graph is stored
std::string getDir(std::string filename) {
    bool firstHashTag = false;
    bool startReading = false;
    std::string dir;
    for(int i = 0; i < filename.size(); i++) {
        if(filename[i] == '#' && !firstHashTag) {
            firstHashTag = true;
            continue;
        }
        if(filename[i] == '#' && firstHashTag) {
            startReading = true;
            continue;
        }
        if(startReading) {
            if(filename[i] == '.') {
                break;
            }
            dir.push_back(filename[i]);
        }
    }
    return dir;
}

void getGraph(graph_access& G, const std::string& filename) {
    std::string line;

    // open file for reading
    std::ifstream in(filename.c_str());
    if (!in) {
            std::cerr << "Error opening (get Graph)" << filename << std::endl;
            exit(0);
    }

    long nmbNodes;
    long nmbEdges;

    std::getline(in,line);
    //skip comments
    while( line[0] == '%' ) {
            std::getline(in, line);
    }

    int ew = 0;
    std::stringstream ss(line);
    // read in first three numbers
    // number of nodes, number of edges, graph conf. parameter
    ss >> nmbNodes;
    ss >> nmbEdges;
    ss >> ew;

    bool read_ew = false;
    bool read_nw = false;
    // check graph conf. parameter
    if(ew == 1) {
            read_ew = true; //graph has (only) edge weights
    } else if (ew == 11) {
            read_ew = true; // graph has edge weights
            read_nw = true; // and node weights
    } else if (ew == 10) {
            read_nw = true; // graph has (only) node weights
    }
    nmbEdges *= 2; //since we have forward and backward edges

    NodeID node_G;
    G.start_construction(nmbNodes, nmbEdges);
    while(std::getline(in, line)) {
            if (line[0] == '%') { // a comment in the file
                    continue;
            }

            std::stringstream ss(line);

            NodeWeight weight = 1;
            if( read_nw ) {
                    ss >> weight;
            }
            node_G = G.new_node();
            G.setPartitionIndex(node_G, 0);
            G.setNodeWeight(node_G, weight);

            NodeID target;
            while( ss >> target ) {

                    EdgeWeight edge_weight = 1;
                    if( read_ew ) {
                            ss >> edge_weight;
                    }
                    EdgeID e = G.new_edge(node_G, target-1);
                    G.setEdgeWeight(e, edge_weight);
            }
            if(in.eof()) {
                    break;
            }
    }
    G.finish_construction();
}

//checks the indSet returned from Kamis
void check_IndSet(graph_access& G, std::string writeTo, std::string kamisSet, std::string algorithm) {
    // first check the ind set
    NodeID index = 0;
    bool is_maximal = true;
    bool is_independent = true;

    NodeWeight weight_sum = 0;
    NodeWeight total_weight_sum = 0;
    NodeID number_of_nodes = 0;
    NodeID IndSet_neighbor;

    std::ifstream in(kamisSet.c_str());
    std::string line;
    if (!in) {
            std::cerr << "Error opening (check IndSet)" << kamisSet << std::endl;
            exit(0);
    }
    std::vector<bool> IndSet;
    IndSet.reserve(G.number_of_nodes());
    bool temp;
    while(std::getline(in, line)) {
        temp = std::stoi(line);
        IndSet.push_back(temp);
    }

    std::ofstream output;
    output.open(writeTo+".txt", std::ios_base::app);
    output << algorithm << " ";

    for(bool node : IndSet) {
        // check if Ind Set is maximal
        // (every node that is not in the ind set has at least one
        // neighbor in the ind set)
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        // check if Ind Set is independent
        // (Every node in the Ind Set has no neighbor that is also
        // in the Ind Set)
        bool has_IndSet_neighbor = false;
        forall_out_edges(G,e,index) {
            if(G.getEdgeTarget(e) != index) {
                if(IndSet[G.getEdgeTarget(e)]) {
                    has_IndSet_neighbor = true;
                    IndSet_neighbor = G.getEdgeTarget(e);
                    break;
                }
            }
        }endfor
        if(!has_IndSet_neighbor && !node) {
            is_maximal = false;
        }
        if(has_IndSet_neighbor && node) {
            is_independent = false;
        }

        // create information of the Ind set
        total_weight_sum += G.getNodeWeight(index);
        if(node) {
            number_of_nodes++;
            weight_sum += G.getNodeWeight(index);
        }
        index++;
    }

    output << "Max:" << is_maximal << " Ind:" << is_independent;
    output << " Nds:" << number_of_nodes << "/" << index << " (" << ((float)(number_of_nodes)*100.0)/(float)(index) << "%)";
    output << " Wght:" << weight_sum << "/" << total_weight_sum << " (" << ((float)(weight_sum)*100.0)/(float)(total_weight_sum) << "%)";
    output << std::endl;
}
