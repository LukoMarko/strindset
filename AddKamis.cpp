#include <sstream>
#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <fstream>

std::string getFile(std::string path);

// Adds the results of the ind sets from the kamis algorithms (stored in KAmis indSets folder)
// to the results calculated from StrIndSets algorithms
int main(int argn, char **argv) {
    std::ifstream in("paths.txt", std::ifstream::in);
    if (!in) {
            std::cerr << "Error opening paths.txt" << std::endl;
            exit(0);
    }
    std::string path;
    while(std::getline(in, path)) {
        std::cout << path << std::endl;
        std::string file = getFile(path);
        //std::cout << "folder " << getFolder(path) << std::endl;
        //std::cout << "file " << getFile(path) << std::endl;
        //std::cout << "keyword " << getKeyword(path) << std::endl;
        //std::cout << std::endl;
        std::string line;
        std::ifstream kamis("./KaMIS IndSets/"+file+".txt", std::ifstream::in);
        if (!in) {
                std::cerr << "Error opening (Kamis IndSet)" << file << std::endl;
                continue;
        }
        std::ofstream strindset;
        strindset.open(file+".txt", std::ios_base::app);
        while(std::getline(kamis, line)) {
            strindset << line << std::endl;
        }
    }
}


// Takes the path to a graph and returns the filename
std::string getFile(std::string path) {
    unsigned lastBackSlash;
    for(int i = 0; i < path.size(); i++) {
        if(path[i] == '/') {
            lastBackSlash = i;
        }
    }
    std::string file;
    for(unsigned i = lastBackSlash +1; i < path.size(); i++) {
            file.push_back(path[i]);
    }
    return file;
}
